<?php
/*
  Template Name: Index Nascosta
 */
$home_configuration = new Paranix_PageManager_Home();

Paranix_Core_LayoutManager::include_partials('header.php');

?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <div class="container">
        <?php Paranix_Core_LayoutManager::include_partials('competition.php', 'index'); ?>
        <hr class="cloud" />
        <?php Paranix_Core_LayoutManager::include_partials('project.php', 'index'); ?>
        <hr class="cloud" />
        <?php Paranix_Core_LayoutManager::include_partials('imageboxes.php', 'index'); ?>
        <hr class="cloud" />
        <?php Paranix_Core_LayoutManager::include_partials('credits-footer.php'); ?>
        <hr/><hr/>
    </div>
    <?php
        Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>