<?php
/*
  Template Name: Funzioni utente
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

$action = $_GET['action'];

$action_accepted = array(
    'applogin',
    'login',
    'logout',
    'recoverpassword',
    'register',
    'resetpassword',
    'update_user_profile',
    'useractivation'
);

if(!in_array($action, $action_accepted)){
    wp_redirect( home_url() );
}

switch ($action){
    
    case 'login':
        $user = new Paranix_Core_UserManager();
        $email = filter_input(INPUT_POST, 'paranix_login_email', FILTER_SANITIZE_EMAIL);
        $password = filter_input(INPUT_POST, 'paranix_user_password', FILTER_SANITIZE_STRING);
        if ($user->login($email, $password)) {
            Paranix_Core_LoaderManager::redirect_to_page_by_title('Area utente');
        }
        else {
            Paranix_Core_FlashMessage::set_message('Login non effettuato. Controlla i dati inseriti!', 'danger');
            Paranix_Core_LoaderManager::redirect_to_page_by_title('Login e Recupera Password');
        }
        break;
        
    case 'logout':
        wp_logout();
        wp_redirect( home_url() );
        break;
    
    case 'register':
        if(Paranix_Core_LoaderManager::is_post()){
            $user_manager = new Paranix_Core_UserManager();
            $paranix_user_id = $user_manager->process_registration($_POST);
            if (is_int($paranix_user_id)) {
                if ($_FILES['paranix_userimage']['name']) {
                    $paranix_photo_contest_obj = new Paranix_Core_PhotoContest();
                    $paranix_photoc_contest_attachment_id = $paranix_photo_contest_obj->upload_image(
                            $_FILES['paranix_userimage'], $_POST['paranix_photo_description'], $paranix_user_id
                    );
                }
                Paranix_Core_EmailManager::send_user_activation($paranix_user_id);
                Paranix_Core_LoaderManager::redirect_to_page_by_title('Landing Post Registrazione');
                exit;
            }
            else{
                wp_safe_redirect( wp_get_referer());                
            }
        }
        break;
    
    case 'recoverpassword':
        $user = new Paranix_Core_UserManager();
        $email = filter_input(INPUT_POST, 'paranix_login_email', FILTER_SANITIZE_EMAIL);
        $userObj = $user->check_user($email);
        if($userObj instanceof WP_User){
            Paranix_Core_FlashMessage::set_message('Ti è stata appena spedita una mail all\'indirizzo con cui ti sei registrato per poter resettare la password.', 'success');
            Paranix_Core_EmailManager::send_lost_password($userObj->ID);
            Paranix_Core_LoaderManager::redirect_to_page_by_title('Login e Recupera Password');
        }
        break;
    
    case 'resetpassword':
        if(Paranix_Core_LoaderManager::is_post()){
            if(is_user_logged_in()){
                $signon = true;
            }
            else{
                $signon = false;
            }
            $email = filter_input(INPUT_POST, 'user_email', FILTER_SANITIZE_EMAIL);
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                $user = get_user_by('email', $email);
                $password = filter_input(INPUT_POST, 'paranix_user_password', FILTER_SANITIZE_STRING);
                $match_password = filter_input(INPUT_POST, 'paranix_user_match_password', FILTER_SANITIZE_STRING);
                if($user instanceof WP_User && $password === $match_password){
                    wp_set_password($password, $user->ID);
                    if($signon){
                        wp_cache_delete($user->ID, 'users');
                        wp_cache_delete($user->user_login, 'userlogins'); 
                        wp_logout();
                        wp_signon(array('user_login' => $user->user_login, 'user_password' => $password));
                    }
                    Paranix_Core_FlashMessage::set_message('La nuova password è stata salvata con successo.', 'success');
                    $referer_id = url_to_postid( wp_get_referer() );
                    $referer_page = get_post($referer_id);
                    if($referer_page->post_title == 'Recupera Password'){
                        Paranix_Core_LoaderManager::redirect_to_page_by_title('Sign up');
                    }
                    else{
                        Paranix_Core_LoaderManager::redirect_to_page_by_title('Area utente');
                    }
                }
                else{
                    Paranix_Core_FlashMessage::set_message('L\'utente non esiste o la password inserita non è corretta.', 'danger');
                    wp_safe_redirect( wp_get_referer());
                }
            }
            else{
                Paranix_Core_FlashMessage::set_message('Per poter modificare la password devi essere loggato o devi cliccare sul link ricevuto nella mail di recupera password.', 'danger');
                wp_safe_redirect( wp_get_referer());
            }
        }
        else{
            wp_redirect( home_url() );
        }
        break;
        
    case 'update_user_profile':
        
        if(Paranix_Core_LoaderManager::is_post()){
            if(is_numeric($_POST['paranix_user_id'])){
                $user_id = $_POST['paranix_user_id'];
                $first_name = filter_input(INPUT_POST, 'paranix_user_firstname', FILTER_SANITIZE_STRING);
                $last_name = filter_input(INPUT_POST, 'paranix_user_lastname', FILTER_SANITIZE_STRING);
                $telephone = filter_input(INPUT_POST, 'paranix_usertel', FILTER_SANITIZE_STRING);
                
                $user = get_user_by('id', $user_id);
                $uid = wp_update_user(array(
                    'ID' => $user_id,
                    'first_name' => $first_name,
                    'last_name' => $last_name
                ));
                
                if(is_int($uid)){
                    if(is_numeric($_POST['paranix_comune_id'])){
                        update_user_meta($user_id, 'paranix_user_comune_id', $_POST['paranix_comune_id']);
                    }
                    if(is_numeric($_POST['paranix_usertel'])){
                        update_user_meta($user_id, 'paranix_user_tel', $_POST['paranix_usertel']);
                    }
                    if(is_numeric($_POST['paranix_school_registrazione'])){
                        update_user_meta($user_id, 'paranix_user_childschool', $_POST['paranix_school_registrazione']);
                        if(strlen($_POST['paranix_user_school_room'])){
                            update_user_meta($user_id, 'paranix_user_school_room', $_POST['paranix_user_school_room']);
                        }
                    }
                }
                
                Paranix_Core_FlashMessage::set_message('I tuoi dati sono stati aggiornati con successo.', 'success');
                wp_safe_redirect( wp_get_referer());
                die;
            }
            
        }
        
        Paranix_Core_FlashMessage::set_message('C\'è stato un errore con la tua richiesta.', 'danger');
        Paranix_Core_LoaderManager::redirect_to_page_by_title('Area utente');
        break;
    case 'useractivation':
        $activation_key = $_GET['activation_key'];
        if(strlen($activation_key) && ! is_user_logged_in()){
            $user = new Paranix_Core_UserManager();
            if($user->confirm_registration($activation_key)){
                Paranix_Core_FlashMessage::set_message('Ora puoi utilizzare il tuo account e caricare foto pidocchiose.', 'success');
                Paranix_Core_LoaderManager::redirect_to_page_by_title('Login e Recupera Password');
            }
        }
        else{
            wp_redirect( home_url() );
        }
        break;
    
}