<?php


class Paranix_AdminPages_Teachers{
    
    public static function init(){
        self::add_role();
        self::teacher_add();
        self::teacher_list();
    }
    
    public static function add_role(){
        add_role( 'teacher', 'Docente', 'None' );
    }

    public static function teacher_add(){
        add_submenu_page(
            Paranix_AdminPages_Teachers::get_parent_slug(),
            'Aggiungi Docente',     // page title
            'Aggiungi Docente',     // menu title
            'manage_options',   // capability
            'paranixteachersadd',     // menu slug
            array('Paranix_AdminPages_Teachers', 'teacher_add_render') // callback function
        );
    }
    
    public static function teacher_add_render(){
        $redirect = '<script type="text/javascript">';
        $redirect .= 'window.location = "/wp-admin/user-new.php"';
        $redirect .= '</script>';
        echo $redirect;
        exit();
    }
    
    
    public static function teacher_list(){
        add_submenu_page(
            Paranix_AdminPages_Teachers::get_parent_slug(),
            'Lista dei Docenti',     // page title
            'Lista dei Docenti',     // menu title
            'manage_options',   // capability
            'paranixteacherslist',     // menu slug
            array('Paranix_AdminPages_Teachers', 'teacher_list_render') // callback function
        );
    }
    
    public static function teacher_list_render(){
        $redirect = '<script type="text/javascript">';
        $redirect .= 'window.location = "/wp-admin/users.php?role=teacher"';
        $redirect .= '</script>';
        echo $redirect;
        exit();
    }
    
    /**
     * 
     * @return string $path
     */
    public static function get_template_path(){
        return get_template_directory().'/admin_pages/html/teachers';
    }


    public static function get_parent_slug(){
        return 'edit.php?post_type='.Paranix_Custom_Post_Types_School::PARANIX_SCHOOL_CPT;
    }
}

add_action( 'admin_menu', array('Paranix_AdminPages_Teachers', 'init') );