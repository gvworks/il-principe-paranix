<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_AdminPages_Classroom{
    
    public static function init(){
        self::add_role();
        self::classromm_add();
    }

    public static function classromm_add(){
        add_submenu_page(
            Paranix_AdminPages_Teachers::get_parent_slug(),
            'Aggiungi classe a scuola',     // page title
            'Aggiungi classe a scuola',     // menu title
            'manage_options',   // capability
            'paranixteachersadd',     // menu slug
            array('Paranix_AdminPages_Teachers', 'teacher_add_render') // callback function
        );
    }
    
    public static function classromm_add_render(){
    }
    
    /**
     * 
     * @return string $path
     */
    public static function get_template_path(){
        return get_template_directory().'/admin_pages/html/classroom';
    }


    public static function get_parent_slug(){
        return 'edit.php?post_type='.Paranix_Custom_Post_Types_School::PARANIX_SCHOOL_CPT;
    }
    
}

