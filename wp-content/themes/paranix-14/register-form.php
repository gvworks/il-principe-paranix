<?php
/*
  Template Name: Registrazione Form
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

if(is_user_logged_in()){
    Paranix_Core_LoaderManager::redirect_to_page_by_title('Area utente');
}

if ($wp_query->query_vars['pagename'] == 'registrazione-utente-scuola') {
    $paranix_registration_form_to_include = 'school.php';
} else {
    $paranix_registration_form_to_include = 'simple.php';
}

switch($wp_query->query_vars['pagename']){
    case 'registrazione-utente-scuola':
        $paranix_registration_form_to_include = 'school.php';
        $form_url_action = 'register';
        break;
    
    case 'registrazione-docenti':
        $paranix_registration_form_to_include = 'teacher.php';
        $form_url_action = 'register';
        break;
    
    default:
        $paranix_registration_form_to_include = 'simple.php';
        $form_url_action = 'register';
        break;
}

$form_post_url = Paranix_Core_LoaderManager::get_page_url_by_title('Utenti - Funzioni speciali', array('action' => $form_url_action));

Paranix_Core_LayoutManager::include_partials('header.php');
wp_enqueue_script('jquery-form-validator', 'http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js', array(), false, true);
wp_enqueue_script('register-check', PARANIX_ASSETS_FOLDER . '/custom/js/register/register.js', array(), false, true);

?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12 boxshadow padding-top-bottom">
            <form accept-charset="UTF-8" class="form-horizontal" method="POST" enctype="multipart/form-data" id="register-form" action="<?= $form_post_url; ?>">
                <?php
                    if ($wp_query->query_vars['pagename'] != 'registrazione-utente-mobile' && $wp_query->query_vars['pagename'] != 'registrazione-docenti') {
                        Paranix_Core_LayoutManager::include_partials('photo.php', 'register');
                    }
                ?>
                <?php
                if (Paranix_Core_FlashMessage::has_message()) {
                    print Paranix_Core_FlashMessage::return_formatted_message();
                }
                ?>
                <?php Paranix_Core_LayoutManager::include_partials($paranix_registration_form_to_include, 'register'); ?>
            </form>
        </div>
    </div>
    <hr/>
    <?php
        Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>
