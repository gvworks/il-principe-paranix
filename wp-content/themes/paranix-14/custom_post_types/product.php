<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_Custom_Post_Types_Product {
    
    const PARANIX_PRODUCT_CPT = 'paranixproduct';
    
    public static function init() {
        $labels = array(
            'name' => _x('Prodotti', 'Prodotti', 'text_domain'),
            'singular_name' => _x('Prodotti', 'Prodotti', 'text_domain'),
            'menu_name' => __('Prodotti', 'text_domain'),
            'parent_item_colon' => __('Parent Prodotti:', 'text_domain'),
            'all_items' => __('Tutti i prodotti', 'text_domain'),
            'view_item' => __('Vedi prodotto', 'text_domain'),
            'add_new_item' => __('Aggiungi nuovo prodotto', 'text_domain'),
            'add_new' => __('Aggiungi nuovo prodotto', 'text_domain'),
            'edit_item' => __('Modifica prodotto', 'text_domain'),
            'update_item' => __('Aggiorna prodotto', 'text_domain'),
            'search_items' => __('Cerca prodotto', 'text_domain'),
            'not_found' => __('Non trovato', 'text_domain'),
            'not_found_in_trash' => __('Non trovato nel cestino', 'text_domain'),
        );
        $args = array(
            'label' => __(self::PARANIX_PRODUCT_CPT, 'text_domain'),
            'description' => __('Prodotti Il Principe Paranix', 'text_domain'),
            'labels' => $labels,
            'supports' => array('title', 'excerpt', 'thumbnail'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 24,
            'menu_icon' => 'dashicons-cart',
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'capability_type' => 'page',
        );
        register_post_type(self::PARANIX_PRODUCT_CPT, $args);
        flush_rewrite_rules();
    }
    
    
}

add_action('init', array('Paranix_Custom_Post_Types_Product', 'init'), 0);