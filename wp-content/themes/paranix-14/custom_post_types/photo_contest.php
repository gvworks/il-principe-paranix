<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_Custom_Post_Types_Contest {
    
    const PARANIX_PHOTO_CONTEST_CPT = 'paranixphotocontest';
    
    public static function init() {
        $labels = array(
            'name' => _x('Contest Fotografico', 'Contest Fotografico', 'text_domain'),
            'singular_name' => _x('Contest Fotografico', 'Contest Fotografico', 'text_domain'),
            'menu_name' => __('Contest Fotografico', 'text_domain'),
            'parent_item_colon' => __('Parent Contest Fotografico:', 'text_domain'),
            'all_items' => __('Tutte le foto', 'text_domain'),
            'view_item' => __('Vedi la foto', 'text_domain'),
            'add_new_item' => __('Aggiungi nuova foto', 'text_domain'),
            'add_new' => __('Aggiungi nuova foto', 'text_domain'),
            'edit_item' => __('Modifica foto', 'text_domain'),
            'update_item' => __('Aggiorna foto', 'text_domain'),
            'search_items' => __('Cerca foto', 'text_domain'),
            'not_found' => __('Non trovata', 'text_domain'),
            'not_found_in_trash' => __('Non trovata nel cestino', 'text_domain'),
        );
        $rewrite = array(
            'slug'                => 'foto-contest',
            'with_front'          => false,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label' => __(self::PARANIX_PHOTO_CONTEST_CPT, 'text_domain'),
            'description' => __('Contest Fotografico Il Principe Paranix', 'text_domain'),
            'labels' => $labels,
            'supports' => array('title', 'excerpt', 'thumbnail'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 21,
            'menu_icon' => 'dashicons-format-image',
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'capability_type' => 'page',
            'rewrite'             => $rewrite
        );
        register_post_type(self::PARANIX_PHOTO_CONTEST_CPT, $args);
        flush_rewrite_rules();
    }
    
    /**
     * Set the childname metabox
     */
    public static function set_photo_likes_metabox(){
        add_meta_box(
                'wpt_paranix_photo_likes', 
                'Voti ricevuti dalla foto', 
                array('Paranix_Custom_Post_Types_Contest', 'wpt_paranix_photo_likes'), 
                self::PARANIX_PHOTO_CONTEST_CPT, 
                'normal', 
                'default'
       );
    }
    
    /**
     * Set the childname metabox
     */
    public static function set_child_name_metabox(){
        add_meta_box(
                'wpt_paranix_child_name', 
                'Nome del bambino sulla foto', 
                array('Paranix_Custom_Post_Types_Contest', 'wpt_paranix_chid_name'), 
                self::PARANIX_PHOTO_CONTEST_CPT, 
                'normal', 
                'default'
       );
    }
    
    /**
     * 
     * @global type $post
     */
    public static function wpt_paranix_photo_likes() {
        global $post;
        echo '<input type="hidden" name="paranix_photo_likes_noncename" id="paranix_photo_likes_noncename" value="' .
        wp_create_nonce(plugin_basename(__FILE__)) . '" />';
        $paranix_photo_likes = get_post_meta($post->ID, Paranix_Core_PhotoContest::LIKES_VAR, true);
        echo '<input type="text" name="paranix_photo_likes" id="paranix_photo_likes"  value="' . $paranix_photo_likes . '" class="widefat" />';
    }
    
    /**
     * 
     * @global type $post
     */
    public static function wpt_paranix_chid_name() {
        global $post;
        echo '<input type="hidden" name="paranix_childname_noncename" id="paranix_childname_noncename" value="' .
        wp_create_nonce(plugin_basename(__FILE__)) . '" />';
        $paranix_childname = get_post_meta($post->ID, 'paranix_childname', true);
        echo '<input type="text" name="paranix_childname" id="paranix_childname"  value="' . $paranix_childname . '" class="widefat" />';
    }
    
    public static function wpt_paranix_chid_name_save($post_id, $post) {
        
        if (!wp_verify_nonce($_POST['paranix_childname_noncename'], plugin_basename(__FILE__))) {
            return $post->ID;
        }

        if (!current_user_can('edit_post', $post->ID))
            return $post->ID;
        
        $paranix_childname['paranix_childname'] = $_POST['paranix_childname'];
        
        foreach ($paranix_childname as $key => $value){
            if ($post->post_type == 'revision')
                return; 
            $value = implode(',', (array) $value);
            if (get_post_meta($post->ID, $key, FALSE)){ // If the custom field already has a value
                update_post_meta($post->ID, $key, $value);
            } else { // If the custom field doesn't have a value
                add_post_meta($post->ID, $key, $value);
            }
            if (!$value)
                delete_post_meta($post->ID, $key); // Delete if blank
        }
    }
    
    public static function wpt_paranix_photo_likes_save($post_id, $post) {
        
        if (!wp_verify_nonce($_POST['paranix_photo_likes_noncename'], plugin_basename(__FILE__))) {
            return $post->ID;
        }

        if (!current_user_can('edit_post', $post->ID))
            return $post->ID;
        
        $paranix_photo_likes[Paranix_Core_PhotoContest::LIKES_VAR] = $_POST['paranix_photo_likes'];
        
        foreach ($paranix_photo_likes as $key => $value){
            if ($post->post_type == 'revision')
                return; 
            $value = implode(',', (array) $value);
            if (get_post_meta($post->ID, $key, FALSE)){ // If the custom field already has a value
                update_post_meta($post->ID, $key, $value);
            } else { // If the custom field doesn't have a value
                add_post_meta($post->ID, $key, $value);
            }
            if (!$value)
                delete_post_meta($post->ID, $key); // Delete if blank
        }
    }


    public static function set_fields_post_list( $columns ) {
        $columns['photo_contest'] = 'Foto caricata';
        $columns['user'] = 'Utente';
        $columns['likes'] = 'Voti totali';
        return $columns;
    }
    
    public static function show_fields_post_list( $column, $post_id ) {
        
        $post = get_post($post_id);
        
        if($post->post_type == self::PARANIX_PHOTO_CONTEST_CPT){
            switch ( $column ) {
                case 'photo_contest':
                    echo the_post_thumbnail( 'thumbnail' );
                    break;
                
                case 'user':
                    echo get_the_author();
                    break;
                
                case 'likes':
                    $likes = get_post_meta($post_id, Paranix_Core_PhotoContest::LIKES_VAR, true);
                    echo $likes;
                    break;
            }
        }
        
    }
    
    public static function set_column_sortable( $post_columns ) {
        $post_columns = array(
            'user' => 'user',
            'likes' => 'likes'
            );

        return $post_columns;
    }
    
    public static function set_contest_options(){
        add_submenu_page(
                Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT,
                'Box Pidocchissime', 
                'Box Pidocchissime', 
                'manage_options', 
                'aaaaa-options'
        );
    }
}

add_action('init', array('Paranix_Custom_Post_Types_Contest', 'init'), 0);

add_filter('manage_'.Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT.'_posts_columns' , array('Paranix_Custom_Post_Types_Contest', 'set_fields_post_list') );
add_filter( 'manage_edit-'.Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT.'_sortable_columns', array('Paranix_Custom_Post_Types_Contest', 'set_column_sortable') );

add_action( 'manage_posts_custom_column' , array('Paranix_Custom_Post_Types_Contest', 'show_fields_post_list'), 10, 2 ); 

add_action('do_meta_boxes', array('Paranix_Custom_Post_Types_Contest', 'set_child_name_metabox'));
add_action('do_meta_boxes', array('Paranix_Custom_Post_Types_Contest', 'set_photo_likes_metabox'));

add_action('save_post', array('Paranix_Custom_Post_Types_Contest', 'wpt_paranix_chid_name_save'), 1, 2);
add_action('save_post', array('Paranix_Custom_Post_Types_Contest', 'wpt_paranix_photo_likes_save'), 1, 2);

add_action('admin_menu', array('Paranix_Custom_Post_Types_Contest', 'set_contest_options'));