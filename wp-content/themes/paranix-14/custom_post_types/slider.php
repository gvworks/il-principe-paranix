<?php

define('PARANIX_SLIDER_CPT', 'paranixslider');

function paranix_slider_cpt() {

    $labels = array(
        'name' => _x('Slides', 'Slider General Name', 'text_domain'),
        'singular_name' => _x('Slider', 'Slider Singular Name', 'text_domain'),
        'menu_name' => __('Slider', 'text_domain'),
        'parent_item_colon' => __('Parent slide:', 'text_domain'),
        'all_items' => __('Tutte le slide', 'text_domain'),
        'view_item' => __('Vedi la slide', 'text_domain'),
        'add_new_item' => __('Aggiungi nuova slide', 'text_domain'),
        'add_new' => __('Aggiungi nuova', 'text_domain'),
        'edit_item' => __('Modifica slide', 'text_domain'),
        'update_item' => __('Aggiorna slide', 'text_domain'),
        'search_items' => __('Cerca slide', 'text_domain'),
        'not_found' => __('Non trovata', 'text_domain'),
        'not_found_in_trash' => __('Non trovata nel cestino', 'text_domain'),
    );
    $args = array(
        'label' => __(PARANIX_SLIDER_CPT, 'text_domain'),
        'description' => __('Slider Description', 'text_domain'),
        'labels' => $labels,
        'supports' => array('title', 'excerpt', 'thumbnail',),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 23,
        'menu_icon' => 'dashicons-welcome-view-site',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type(PARANIX_SLIDER_CPT, $args);
    flush_rewrite_rules();
}

add_action('do_meta_boxes', 'paranix_slider_cpt_change_image_position');

function paranix_slider_cpt_change_image_position() {
    remove_meta_box('postimagediv', 'slider', 'side');
    add_meta_box('postimagediv', __('Imposta slide'), 'post_thumbnail_meta_box', PARANIX_SLIDER_CPT, 'normal', 'high');
}

add_action('add_meta_boxes', 'paranix_slider_cpt_add_url_metaboxes');

// Add the Events Meta Boxes
function paranix_slider_cpt_add_url_metaboxes() {
    add_meta_box('wpt_slides_paranix_slider_url', 'Url della slide', 'wpt_slides_paranix_slider_url', PARANIX_SLIDER_CPT, 'normal', 'default');
}

// The Event Location Metabox
function wpt_slides_paranix_slider_url() {
    global $post;

    echo '<input type="hidden" name="paranix_slider_url_noncename" id="paranix_slider_url_noncename" value="' .
    wp_create_nonce(plugin_basename(__FILE__)) . '" />';

    $paranix_slider_url = get_post_meta($post->ID, 'paranix_slider_url', true);
    echo '<input type="text" name="paranix_slider_url" value="' . $paranix_slider_url . '" class="widefat" />';
}

function wpt_save_paranix_slider_url_meta($post_id, $post) {
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if (!wp_verify_nonce($_POST['paranix_slider_url_noncename'], plugin_basename(__FILE__))) {
        return $post->ID;
    }
    // Is the user allowed to edit the post or page?
    if (!current_user_can('edit_post', $post->ID))
        return $post->ID;
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    $events_meta['paranix_slider_url'] = $_POST['paranix_slider_url'];
    // Add values of $events_meta as custom fields
    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
        if ($post->post_type == 'revision')
            return; // Don't store custom data twice
        $value = implode(',', (array) $value); // If $value is an array, make it a CSV (unlikely)
        if (get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if (!$value)
            delete_post_meta($post->ID, $key); // Delete if blank
    }
}

add_action('save_post', 'wpt_save_paranix_slider_url_meta', 1, 2);

add_action('add_meta_boxes', 'paranix_slider_cpt_add_order_metaboxes');

// Add the Events Meta Boxes
function paranix_slider_cpt_add_order_metaboxes() {
    add_meta_box('wpt_slides_paranix_slider_order', 'Ordine della slide', 'wpt_slides_paranix_slider_order', PARANIX_SLIDER_CPT, 'normal', 'default');
}

// The Event Location Metabox
function wpt_slides_paranix_slider_order() {
    global $post;

    echo '<input type="hidden" name="paranix_slider_order_noncename" id="paranix_slider_order_noncename" value="' .
    wp_create_nonce(plugin_basename(__FILE__)) . '" />';

    $paranix_slider_url = get_post_meta($post->ID, 'paranix_slider_order', true);
    echo '<input type="text" name="paranix_slider_order" value="' . $paranix_slider_url . '" class="widefat" />';
}

function wpt_save_paranix_slider_order_meta($post_id, $post) {
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if (!wp_verify_nonce($_POST['paranix_slider_order_noncename'], plugin_basename(__FILE__))) {
        return $post->ID;
    }
    // Is the user allowed to edit the post or page?
    if (!current_user_can('edit_post', $post->ID))
        return $post->ID;
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    $events_meta['paranix_slider_order'] = $_POST['paranix_slider_order'];
    // Add values of $events_meta as custom fields
    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
        if ($post->post_type == 'revision')
            return; // Don't store custom data twice
        $value = implode(',', (array) $value); // If $value is an array, make it a CSV (unlikely)
        if (get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if (!$value)
            delete_post_meta($post->ID, $key); // Delete if blank
    }
}

add_action('save_post', 'wpt_save_paranix_slider_order_meta', 1, 2);

add_action('init', 'paranix_slider_cpt', 0);
