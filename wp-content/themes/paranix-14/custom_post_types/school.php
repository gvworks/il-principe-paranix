<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_Custom_Post_Types_School {

    const PARANIX_SCHOOL_CPT = 'paranixschool';

    public static function init() {
        $labels = array(
            'name' => _x('Scuole aderenti al progetto', 'Scuola', 'text_domain'),
            'singular_name' => _x('Scuola aderente al progetto', 'Scuola', 'text_domain'),
            'menu_name' => __('Scuole', 'text_domain'),
            'parent_item_colon' => __('Parent Scuola:', 'text_domain'),
            'all_items' => __('Tutte le scuole', 'text_domain'),
            'view_item' => __('Vedi la scuole', 'text_domain'),
            'add_new_item' => __('Aggiungi nuova scuola', 'text_domain'),
            'add_new' => __('Aggiungi nuova scuola', 'text_domain'),
            'edit_item' => __('Modifica scuola', 'text_domain'),
            'update_item' => __('Aggiorna scuola', 'text_domain'),
            'search_items' => __('Cerca scuola', 'text_domain'),
            'not_found' => __('Non trovata', 'text_domain'),
            'not_found_in_trash' => __('Non trovata nel cestino', 'text_domain'),
        );
        $args = array(
            'label' => __(self::PARANIX_SCHOOL_CPT, 'text_domain'),
            'description' => __('Scuole aderenti al progetto', 'text_domain'),
            'labels' => $labels,
            'supports' => array('title', 'excerpt', 'thumbnail'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 22,
            'menu_icon' => 'dashicons-welcome-learn-more',
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'capability_type' => 'page',
        );
        register_post_type(self::PARANIX_SCHOOL_CPT, $args);
        flush_rewrite_rules();
    }

    /**
     * Set the childname metabox
     */
    public static function set_school_comune_metabox() {
        add_meta_box(
                'wpt_paranix_school_cpt_comune', 
                'Comune di appartenenza scuola',
                array('Paranix_Custom_Post_Types_School', 'wpt_paranix_school_comune'), 
                self::PARANIX_SCHOOL_CPT, 
                'normal', 
                'default'
        );
    }

    public static function wpt_paranix_school_comune() {
        global $post;
        echo '<input type="hidden" name="paranix_school_comune_noncename" id="paranix_school_comune_noncename" value="' .
        wp_create_nonce(plugin_basename(__FILE__)) . '" />';

        $paranix_school_comune_id = get_post_meta($post->ID, 'paranix_school_comune_id', true);
        $paranix_comune_name = paranix_data_get_comune($paranix_school_comune_id);
        
        echo '<input type="hidden" name="paranix_school_comune_id" id="paranix_school_comune_id" value="' . $paranix_school_comune_id . '" class="hide" />';
        echo '<input type="text" name="paranix_school_comune" id="paranix_school_comune"  value="' . $paranix_comune_name[0]->name . '" class="widefat" />';
    }

    public static function wpt_paranix_school_save($post_id, $post) {
        // verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times
        if (! wp_verify_nonce($_POST['paranix_school_comune_noncename'], plugin_basename(__FILE__))) {
            return $post->ID;
        }
        // Is the user allowed to edit the post or page?
        if (!current_user_can('edit_post', $post->ID))
            return $post->ID;
        // OK, we're authenticated: we need to find and save the data
        // We'll put it into an array to make it easier to loop though.
        $comune_meta['paranix_school_comune_id'] = $_POST['paranix_school_comune_id'];
        // Add values of $comune_meta as custom fields
        foreach ($comune_meta as $key => $value) { // Cycle through the $comune_meta array!
            if ($post->post_type == 'revision')
                return; // Don't store custom data twice
            $value = implode(',', (array) $value); // If $value is an array, make it a CSV (unlikely)
            if (get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
                update_post_meta($post->ID, $key, $value);
            } else { // If the custom field doesn't have a value
                add_post_meta($post->ID, $key, $value);
            }
            if (!$value)
                delete_post_meta($post->ID, $key); // Delete if blank
        }
        
        $paranix_classrooms = $_POST['paranix_classrooms'];
        $paranix_teachers = $_POST['paranix_classrooms_teacher'];
        
        $return_array = array();
        
        foreach ($paranix_classrooms as $index => $classroom){
            $return_array[] = array('classroom' => $classroom, 'teacher' => $paranix_teachers[$index]);
        }
        
        delete_post_meta($post->ID, 'paranix_school_classroom_teachers');
        update_post_meta($post->ID, 'paranix_school_classroom_teachers', $return_array);
        
    }
    
    public static function set_school_classrooms_metabox() {
        add_meta_box(
                'wpt_paranix_school_cpt_classrooms', 
                'Aule',
                array('Paranix_Custom_Post_Types_School', 'wpt_paranix_school_classrooms'), 
                self::PARANIX_SCHOOL_CPT, 
                'normal', 
                'default'
        );
    }
    
    public static function wpt_paranix_school_classrooms() {
        global $post;
        
        echo '<input class="paranix_classrooms_add" type="text" value="" />';
        echo '<select id="teachers" name="teachers">';
        echo '<option>Scegli il docente</option>';
        Paranix_Custom_Post_Types_School::get_teacher_select_option();
        echo '</select>';
        echo '<button class="addclassrooms button button-primary button-large">Aggiungi classe</button>';
        echo '<div id="classrooms">';
        $classrooms = get_post_meta($post->ID, 'paranix_school_classroom_teachers');
        foreach ($classrooms[0] as $classroom){
            echo '<span class="classroom_item">';
            echo '<input name="paranix_classrooms[]" type="text" value="'.$classroom['classroom'].'" />';
            echo '<select id="teachers" name="paranix_classrooms_teacher[]">';
            Paranix_Custom_Post_Types_School::get_teacher_select_option($classroom['teacher']);
            echo '</select>';
            echo '<a href="#" class="removethis">x</a>';
            echo '</span>';
        }
        echo '</div>';
    }
    
    public static function get_teacher_select_option($selected = null){
        $teachers_args = array('role' => 'teacher');
        $teachers = get_users($teachers_args);
        foreach($teachers as $teacher){
            $is_selected = ($selected == $teacher->ID) ? 'selected' : '';
            echo '<option value="'.$teacher->ID.'" '.$is_selected.'>'.$teacher->first_name.' '.$teacher->last_name.'</option>';
        }
    }


    public static function school_admin_scripts(){
        wp_enqueue_script('jquery-ui-autocomplete', '', array('jquery-ui-widget', 'jquery-ui-position'), '1.8.6');
        wp_enqueue_script('classrooms', PARANIX_ASSETS_FOLDER.'/custom/js/wp-admin/school/classrooms.js', array(), false, true);
        wp_enqueue_script('comune-autocomplete', PARANIX_ASSETS_FOLDER.'/custom/js/wp-admin/comuni/comuni.js', array(), false, true);
        wp_enqueue_style('classrooms-style', PARANIX_ASSETS_FOLDER.'/custom/css/wp-admin/school/classrooms.css');
    }

}

add_action('init', array('Paranix_Custom_Post_Types_School','init'), 0);
add_action('admin_init', array('Paranix_Custom_Post_Types_School','school_admin_scripts'));
add_action('do_meta_boxes', array('Paranix_Custom_Post_Types_School','set_school_comune_metabox'));
add_action('do_meta_boxes', array('Paranix_Custom_Post_Types_School','set_school_classrooms_metabox'));
add_action('save_post', array('Paranix_Custom_Post_Types_School','wpt_paranix_school_save'), 1, 2);

