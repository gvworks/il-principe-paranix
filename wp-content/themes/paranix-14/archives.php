<?php
/*
  Template Name: Archivio
 */

Paranix_Core_LayoutManager::include_partials('header.php');
?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
        $slug_category = $_GET['category'];
        $args = array(
            'posts_per_page' => -1,
            'offset' => 0,
            'category_name' => $slug_category,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        $posts = get_posts($args);
        $category_name = get_category_by_slug($slug_category);
    ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12">
            <h3 class="homepage">Articoli della categoria: <?= $category_name->name; ?></h3>
            <?php Paranix_Core_LayoutManager::include_partials('loop.php', 'archives'); ?>
        </div>
    </div>
    <?php
        Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>