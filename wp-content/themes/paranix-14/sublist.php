<?php
/*
  Template Name: Sottopagina concorso
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

Paranix_Core_LayoutManager::include_partials('header.php');

$sorting_array = array(
        'method' => array('date', Paranix_Core_PhotoContest::LIKES_VAR),
        'direction' => array('ASC', 'DESC')
);

$sort_method = (in_array($_GET['sorting_method'], $sorting_array['method'])) ? $_GET['sorting_method'] : 'date';
$sort_direction = (in_array($_GET['sorting_direction'], $sorting_array['direction'])) ? $_GET['sorting_direction'] : 'DESC';

$sorting_date_button = '?sorting_method=date&sorting_direction=';
$sorting_date_button .= ($sort_direction == 'DESC') ? 'ASC' : 'DESC'; 

$sorting_likes_button = '?sorting_method='.Paranix_Core_PhotoContest::LIKES_VAR.'&sorting_direction=';
$sorting_likes_button .= ($sort_direction == 'DESC') ? 'ASC' : 'DESC'; 

$page = ($_GET['page']) ? $_GET['page'] : 0;
$ppp = 10;

$photo_contest = new Paranix_Core_PhotoContest();
$photo_contest_list = $photo_contest->contest_list($ppp, $page, $sort_method, $sort_direction);

wp_enqueue_style('mnemosi-lightbox-css', PARANIX_ASSETS_FOLDER.'/custom/css/lightbox/lightbox.css');
wp_enqueue_script('mnemosi-lightbox', PARANIX_ASSETS_FOLDER.'/custom/js/lightbox/lightbox.js', array(), false, true);
wp_enqueue_script('list', PARANIX_ASSETS_FOLDER.'/custom/js/list/list.js', array(), false, true);
?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12 boxshadow">
            <?php Paranix_Core_LayoutManager::include_partials('breadcrumb.php', 'list'); ?>
            <div class="col-md-12">&nbsp;</div>
            <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        Paranix_Core_LayoutManager::include_partials('new.php', 'single');
                    }
                }
            ?>
            <div class="col-md-12">&nbsp;</div>
        </div>
    </div>
    <hr/>
    <?php
        Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>