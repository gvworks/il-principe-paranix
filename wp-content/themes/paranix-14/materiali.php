<?php
/*
  Template Name: Materiali
 * 
 */
Paranix_Core_LayoutManager::include_partials('header.php');
wp_enqueue_script('login-check', PARANIX_ASSETS_FOLDER.'/custom/js/materiali/materiali.js', array(), false, true);
?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
<hr/>
    <div class="container-fluid">
        <?php Paranix_Core_LayoutManager::include_partials('tabs.php', 'materiali'); ?>
    </div>
    <hr/>
    <?php Paranix_Core_LayoutManager::include_partials('footer.php'); ?>
</body>
</html>