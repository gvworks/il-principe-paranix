<?php
/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
Paranix_Core_LayoutManager::include_partials('header.php');
$type = Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT;
$args = array (
    'post_type' => $type,
    'post_status' => 'publish',
    'paged' => $paged,
    'posts_per_page' => 1,
    'ignore_sticky_posts'=> 1
);
$temp = $wp_query; // assign ordinal query to temp variable for later use  
$wp_query = null;
$wp_query = new WP_Query($args); 
?>
<body>
    <?php
    Paranix_Core_LayoutManager::include_partials('nav.php');
    Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <hr/>
    <div class="container">
        <?php
                if ($wp_query->have_posts()) {
                    while ($wp_query->have_posts()) {
                        $wp_query->the_post();
                        Paranix_Core_LayoutManager::include_partials('paranixphotocontest.php', 'single');
                    }
                }
            ?>
    </div>
    <hr/>
    <?php
    Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>
