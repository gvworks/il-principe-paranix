<?php
/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
Paranix_Core_LayoutManager::include_partials('header.php');
?>
<body>
    <?php
    Paranix_Core_LayoutManager::include_partials('nav.php');
    Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-sm-12 col-md-8 col-md-offset-2 castle-before">
            <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        Paranix_Core_LayoutManager::include_partials('simple.php', 'single');
                    }
                }
            ?>
        </div>
    </div>
    <hr/>
    <?php
    Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>
