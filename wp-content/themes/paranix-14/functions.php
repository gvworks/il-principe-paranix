<?php

define('PARANIX_ASSETS_FOLDER', '/assets');

add_action('init', 'paranix_start_session', 1);

remove_action('wp_head', 'wp_generator');

function paranix_start_session() {
    if( !session_id()) {
        session_start();
    }
    return false;
}

add_theme_support( 'post-thumbnails' );

include_once dirname(__FILE__).'/core/loader_manager.php';

$paranix_core_loader_manager = new Paranix_Core_LoaderManager();
Paranix_Core_ThemeOptionManager::load();