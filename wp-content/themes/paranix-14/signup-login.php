<?php
/*
  Template Name: Registrazione e Login
 */
/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
if (is_user_logged_in()) {
    //go to area utente
    Paranix_Core_LoaderManager::redirect_to_page_by_title('Area utente');
}

Paranix_Core_LayoutManager::include_partials('header.php');
wp_enqueue_script('login-check', PARANIX_ASSETS_FOLDER . '/custom/js/signin/signin.js', array(), false, true);
?>
<body>
    <?php Paranix_Core_LayoutManager::include_partials('nav.php'); ?>
    <?php Paranix_Core_LayoutManager::include_partials('carousel.php'); ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12 boxshadow">
            <?php Paranix_Core_LayoutManager::include_partials('breadcrumb.php', 'list'); ?>
            <div class="col-sm-12 col-md-8 col-md-offset-2 castle-before">
                <?php Paranix_Core_LayoutManager::include_partials('svg-castle.php', 'check'); ?>
            </div>
            <div class="col-sm-12 col-md-8 col-md-offset-2 padding-top-bottom castle-after col-mabottom">
                <div id='main-checkpage-content'>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <?php Paranix_Core_LayoutManager::include_partials('register.php', 'check'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 bordered-top">
                        <?php Paranix_Core_LayoutManager::include_partials('login.php', 'check'); ?>
                    </div>
                </div>
                <div id="lost-password" class="col-md-12 bordered-top hide">
                    <?php Paranix_Core_LayoutManager::include_partials('lost-password.php', 'check'); ?>
                </div>
                <div id="choose-registration-type" class="col-md-12 bordered-top hide">
                    <?php Paranix_Core_LayoutManager::include_partials('choose-registration-type.php', 'check'); ?>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <?php Paranix_Core_LayoutManager::include_partials('footer.php'); ?>
</body>
</html>
