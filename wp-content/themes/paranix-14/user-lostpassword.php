<?php
/*
  Template Name: Recupera Password
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

//Controllo che l'utente che sta visitando questa pagina ne viene dalla pagina di registrazione.
if(is_user_logged_in()){
    //go to area utente
    Paranix_Core_LoaderManager::redirect_to_page_by_title('Area utente');
}

Paranix_Core_LayoutManager::include_partials('header.php');
Paranix_Core_LayoutManager::include_partials('nav.php');
Paranix_Core_LayoutManager::include_partials('carousel.php');

$query_string = Paranix_Core_UserManager::url_decodified_user_email($_GET['key']);
$user_email = $query_string['email'];
?>
<hr/>
    <div class="container-fluid">
        <div class="col-sm-12 col-md-8 col-md-offset-2 castle-before">
            <h3 class="bigh3title">Modifica la password</h3>
            <hr/>
            <p>Inserisci la nuova password come richiesto.</p>
            <?php
                if (Paranix_Core_FlashMessage::has_message()) {
                    print Paranix_Core_FlashMessage::return_formatted_message();
                }
            ?>
            <div class="well col-mabottom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile" data-toggle="tab">Password</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <?php $tab_pane_active = 'password'; ?>
                    <?php Paranix_Core_LayoutManager::include_partials('password.php', 'user-area'); ?>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <?php
    Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>
