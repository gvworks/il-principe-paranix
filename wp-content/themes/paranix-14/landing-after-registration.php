<?php
/*
  Template Name: Landing Post Registrazione
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

//Controllo che l'utente che sta visitando questa pagina ne viene dalla pagina di registrazione.
if(! Paranix_Core_UserManager::came_from_registration(TRUE) || is_user_logged_in()){
    wp_redirect(get_home_url());
}

Paranix_Core_LayoutManager::include_partials('header.php');
?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-sm-12 col-md-8 col-md-offset-2 castle-before">
            <?php Paranix_Core_LayoutManager::include_partials('svg-castle.php', 'check'); ?>
        </div>
        <div class="col-sm-12 col-md-8 col-md-offset-2 padding-top-bottom castle-after col-mabottom">
            <?php Paranix_Core_LayoutManager::include_partials('message.php', 'landing-after-registration'); ?>
        </div>
    </div>
    <hr/>
    <?php
        Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>
