<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_PageManager_Home{
    
    public function get_main_post(){
        $main_box_title = ot_get_option('paranix_box_main_post_title');
        $main_box_post_id = ot_get_option('paranix_box_main_post_post_id');
        $main_post = get_post($main_box_post_id);
        $main_box_links = ot_get_option('paranix_box_main_post_links');
        $main_box_image = wp_get_attachment_image_src( get_post_thumbnail_id( $main_post->ID ), 'default-medium-cropped' );
        
        return array(
            'box_title' => $main_box_title,
            'post_excerpt' => $main_post->post_excerpt,
            'post_link' => get_permalink( $main_post->ID ),
            'post_image' => $main_box_image[0],
            'box_links' => $main_box_links
        );
    }
    
    public function get_app_box(){
        $box_app_image_src = ot_get_option('paranix_home_app_image');
        $box_app_post_content = ot_get_option('paranix_home_app_text');
        
        return array(
            'box_image' => $box_app_image_src,
            'post_content' => $box_app_post_content
        );
    }
    
    public function get_news(){
        $news_box_title = ot_get_option('paranix_home_news_title');
        $news_box_category_id = ot_get_option('paranix_home_news_category');
        $news_box_number_of_post = ot_get_option('paranix_home_news_number');
        $args = array(
            'posts_per_page' => $news_box_number_of_post,
            'offset' => 0,
            'category' => $news_box_category_id,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        return array(
            'box_title' => $news_box_title,
            'posts' => get_posts($args),
            'more_link' => '#'
        );
    }
    
    public function get_image_boxes(){
        return ot_get_option('paranix_home_images_items');
    }
}

