<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_PageManager_Global{
    
    public static function get_facebook_url(){
        return ot_get_option('paranix_social_network_facebook_url');
    }
    
    public static function get_credits_footer(){
        return ot_get_option('paranix_block_credits');
    }
    
    public static function get_regolamento_concorso(){
        return ot_get_option('paranix_regolamento_del_concorso');
    }
    
    public static function get_utilizzo_dati(){
        return ot_get_option('paranix_utilizzo_dati');
    }
    
}