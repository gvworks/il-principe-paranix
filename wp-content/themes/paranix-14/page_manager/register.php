<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_PageManager_Register{
    
    public static function get_regolamento_concorso(){
        return ot_get_option('paranix_regolamento_del_concorso');
    }
    
    public static function get_utilizzo_dati(){
        return ot_get_option('paranix_utilizzo_dati');
    }
    
}