<?php require_once 'partials/header.php'; ?>
<body>
    <div class="container">
        <div class="span3 well">
            <legend>Test upload App</legend>
            <form accept-charset="UTF-8" action="/app-login/" method="post" enctype="multipart/form-data">
                <input class="span3" name="username" placeholder="Username" type="text">
                <input class="span3" name="password" placeholder="Password" type="password"> 
                <hr/>
                <input type="file" name="photo" id="photo"  multiple="false" />
                <?php wp_nonce_field('my_image_upload', 'my_image_upload_nonce'); ?>
                <input class="span3" name="paranix_childname" placeholder="Nome Bimbo" type="text">
                <input class="span3" name="paranix_photo_description" placeholder="Descrizione della foto" type="text"> 
                <hr/>
                <button class="btn btn-success" type="submit">Testa upload</button>
            </form>
        </div>
    </div>
    <?php require_once 'partials/footer.php'; ?>
</body>
</html>