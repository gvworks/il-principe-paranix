<?php require_once  'partials/header.php' ; ?>
<body>
	<?php require 'partials/nav.php'; ?>
	<?php require_once 'partials/carousel.php'; ?>
	<div class="container">
		<?php require_once 'partials/competition.php'; ?>
		<hr class="cloud" />
		<?php require_once 'partials/project.php'; ?>
		<hr class="cloud" />
		<?php require_once 'partials/cartoon-and-game.php'; ?>
		<hr class="cloud" />
		<?php require_once 'partials/credits-footer.php'; ?>
		<hr/><hr/>
	</div>
	<?php require 'partials/nav.php'; ?>
	<?php require_once 'partials/footer.php'; ?>
</body>
</html>