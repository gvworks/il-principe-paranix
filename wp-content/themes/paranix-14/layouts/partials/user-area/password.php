<?php 
    global $tab_pane_active, $user_email;
    $active_class = ($tab_pane_active == 'password') ? 'active' : ''; 
    $form_post_url = Paranix_Core_LoaderManager::get_page_url_by_title('Utenti - Funzioni speciali', array('action' => 'resetpassword'));
?>
<form id="tab2" class="form-horizontal" action="<?= $form_post_url; ?>" method="POST">
    <div class="col-lg-12">
        <label class="control-label" for="paranix_user_email">Nuova password</label>
        <div class="controls">
            <input type="password" class="form-control" id="paranix_user_password" name="paranix_user_password" value=""/>
        </div>
    </div>
    <div class="col-lg-12">
        <label class="control-label" for="paranix_user_email">Ripeti password</label>
        <div class="controls">
            <input type="password" class="form-control" id="paranix_user_match_password" name="paranix_user_match_password" value=""/>
        </div>
    </div>
    <input type="hidden" name="user_email" id="user_email" value="<?= $user_email; ?>" />
    <div class="col-lg-12">&nbsp;</div>
    <div class="pull-right">
        <button class="btn btn-primary">Aggiorna</button>
    </div>
</form>