<div class="container-fluid">
    <div class="well well-sm">
        <strong>Le tue foto</strong>
        <div class="btn-group">
            <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Griglia</a>
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>Lista</a>
        </div>
    </div>
    <div id="products" class="row list-group">
        <?php foreach(Paranix_Core_PhotoContest::get_user_photo(get_current_user_id()) as $index => $photo): ?>
        <div class="item  col-xs-12 col-lg-4">
            <div class="thumbnail">
                <img class="img-responsive" src="<?= $photo['image']; ?>" alt="" />
                <div class="caption text-center">
                    <h4 class="group inner list-group-item-heading"><?= $photo['childname']; ?></h4>
                    <small class="text-primary"><?= $photo['insert_date']; ?></small><br/>
                    <?php
                        if($photo['status'] == 'active'){
                            $btn_status = 'text-success';
                            $btn_text = 'Foto approvata!';
                            $icon = 'glyphicon glyphicon-ok';
                        }
                        else{
                            $btn_status = 'text-danger';
                            $btn_text = 'Foto in moderazione!';
                            $icon = 'glyphicon glyphicon-flag';
                        }
                    ?>
                    <strong class="<?= $btn_status; ?>"><i class="<?= $icon; ?>"></i> <?= $btn_text; ?></strong>
                    <?php if($photo['status'] == 'active'): ?>
                    <span class="text-success">Voti totali: <?= $photo['likes']; ?></span>
                    <?php endif; ?>
                    <hr/>
                    <a class="btn btn-primary" data-toggle="modal" data-target="#photo_contest_<?= $index; ?>"><span class="glyphicon glyphicon-pencil"></span> Modifica</a>&nbsp;
                    <a class="btn btn-danger remove-photo" data-target="<?= $photo['photo_id']; ?>"><span class="glyphicon glyphicon-remove"></span> Elimina</a>
                </div>
            </div>
        </div>
        <div class="modal fade" id="photo_contest_<?= $index; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel">Modifica le info della foto.</h4>
                </div>
                  <form method="POST" class="update_data">
                <div class="modal-body">

                        <!-- Form Name -->

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="control-group">
                                    <label class="control-label" for="paranix_childname">Nome del bambino <span class="danger">*</span></label>
                                    <input type="text" class="form-control" id="paranix_childname" name="paranix_childname" 
                                           data-validation="required" 
                                           data-validation-error-msg="Il campo nome del bambino non può essere vuoto"
                                           placeholder="Nome del bambino" value="<?= $photo['childname']; ?>"/>
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" class="hide" id="photo_id" name="photo_id"  value="<?= $photo['photo_id']; ?>"/>
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="control-group">
                                    <label class="control-label" for="paranix_photo_description">Descrizione della foto <span class="danger">*</span></label>
                                    <div class="controls">
                                        <textarea id="paranix_photo_description"  name="paranix_photo_description" class="form-control" rows=7"><?= $photo['description']; ?></textarea><br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="control-group">
                                    <label class="control-label" for="paranix_photo_description">Descrizione della foto <span class="danger">*</span></label>
                                    <div class="controls">
                                        <input type="text" class="form-control" id="paranix_childname_parent" name="paranix_childname_parent" 
                                           data-validation="required" 
                                           data-validation-error-msg="Il campo nome del bambino non può essere vuoto"
                                           placeholder="Nome del genitore del bambino" value="<?= $photo['parent_child']; ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                  <button type="submit" class="btn btn-primary send_data" data-loading-text="Loading...">Salva i cambiamenti</button>
                </div>
                  </form>
              </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>