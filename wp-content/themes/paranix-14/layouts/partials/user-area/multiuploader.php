<?php

wp_enqueue_script('jquery-upload-file-js', PARANIX_ASSETS_FOLDER . '/custom/js/uploadfile/jquery.uploadfile.min.js', array(), false, true);
wp_enqueue_script('jquery-upload-file-function-js', PARANIX_ASSETS_FOLDER . '/custom/js/uploadfile/function.js', array(), false, true);
wp_enqueue_style('jquery-upload-file-css', PARANIX_ASSETS_FOLDER . '/custom/css/uploadfile/uploadfile.min.css', array(), false, 'all');
wp_enqueue_style('upload-file-css', PARANIX_ASSETS_FOLDER . '/custom/css/uploadfile/style.css', array(), false, 'all');

?>
<div class="col-md-12">
    <hr/>
    <p>Ricorda che puoi decidere di associare un nome e una descrizione a tutte le foto.</p>
    <hr/>
    <div id="multipleupload">Carica foto</div>
</div>