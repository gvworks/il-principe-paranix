<?php
global $current_user;
get_currentuserinfo();
$logout_url = Paranix_Core_LoaderManager::get_page_url_by_title('Utenti - Funzioni speciali', array('action' => 'logout'));
$tabs = array('home', 'password', 'photo-contest', 'photo-contest-upload');
$tab_opened = (in_array($_GET['tab_opened'], $tabs)) ? $_GET['tab_opened'] : 'home';
?>
<div class="well col-mabottom">
    <ul class="nav nav-tabs">
        <li class="<?= ($tab_opened == 'home') ? 'active': ''; ?>">
            <a href="#home" data-toggle="tab">
                <i class="glyphicon glyphicon-user"></i>
                Profilo
            </a>
        </li>
        <li class="<?= ($tab_opened == 'password') ? 'active': ''; ?>">
            <a href="#password" data-toggle="tab">
                <i class="glyphicon glyphicon-asterisk"></i>
                Password
            </a>
        </li>
        <li class="<?= ($tab_opened == 'photo-contest') ? 'active': ''; ?>">
            <a href="#photo-contest" data-toggle="tab">
                <i class="glyphicon glyphicon-picture"></i>
                Le foto che hai caricato
            </a>
        </li>
        <li class="<?= ($tab_opened == 'photo-contest-upload') ? 'active': ''; ?>">
            <a href="#photo-contest-upload" data-toggle="tab">
                <i class="glyphicon glyphicon-camera"></i>
                Carica foto
            </a>
        </li>
        <li class="btn-custom">
            <a href="<?= $logout_url; ?>" class="btn btn-danger">
                <i class="glyphicon glyphicon-off"></i>
                Effettua il logout
            </a>
        </li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane <?= ($tab_opened == 'home') ? 'active': ''; ?> fade-in" id="home">
            <?php Paranix_Core_LayoutManager::include_partials('userprofile.php', 'user-area'); ?>
        </div>
        <div class="tab-pane <?= ($tab_opened == 'password') ? 'active': ''; ?> fade-in" id="password">
            <?php Paranix_Core_LayoutManager::include_partials('password.php', 'user-area'); ?>
        </div>
        <div class="tab-pane <?= ($tab_opened == 'photo-contest') ? 'active': ''; ?> fade-in" id="photo-contest">
            <?php Paranix_Core_LayoutManager::include_partials('photo.php', 'user-area'); ?>
        </div>
        <div class="tab-pane <?= ($tab_opened == 'photo-contest-upload') ? 'active': ''; ?> fade-in" id="photo-contest-upload">
            <?php Paranix_Core_LayoutManager::include_partials('multiuploader.php', 'user-area'); ?>
        </div>
    </div>
</div>
<?php wp_enqueue_script('register-check', PARANIX_ASSETS_FOLDER . '/custom/js/register/register.js', array(), false, true); ?>