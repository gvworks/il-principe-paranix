<?php
    
$user_id = get_current_user_id();

$current_user = get_user_by('id', $user_id);

$telephone = get_user_meta($user_id, 'paranix_user_tel', true);

$comune_id = get_user_meta($user_id, 'paranix_user_comune_id', true);

$comune = paranix_data_get_comune($comune_id);

$scuola_id = get_user_meta($user_id, 'paranix_user_childschool', true);

$aula = '';
$scuola = '';

if(is_int($scuola_id)){
    $aula = get_user_meta($user_id, 'paranix_user_school_room', true);
    $scuola = get_post(array('id' => $scuola_id, 'post_type' => Paranix_Custom_Post_Types_School::PARANIX_SCHOOL_CPT));
    
}

$form_post_url = Paranix_Core_LoaderManager::get_page_url_by_title('Utenti - Funzioni speciali', array('action' => 'update_user_profile'));

wp_enqueue_script('jquery-form-validator', 'http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js', array(), false, true);

?>

<form id="tab" class="form-horizontal" method="post" action="<?= $form_post_url; ?>">
    <input type="hidden" class="hide" name="paranix_user_id" value="<?= $user_id?>" />
    <div class="col-lg-12">
        <label class="control-label" for="paranix_user_firstname">Nome</label>
        <div class="controls">
            <input type="text" class="form-control" id="paranix_user_firstname" name="paranix_user_firstname" value="<?= $current_user->user_firstname; ?>"/>
        </div>
    </div>
    <div class="col-lg-12">
        <label class="control-label" for="paranix_user_lastname">Cognome</label>
        <div class="controls">
            <input type="text" class="form-control" id="paranix_user_lastname" name="paranix_user_lastname" value="<?= $current_user->user_lastname; ?>"/>
        </div>
    </div>
    <div class="col-lg-12">
        <label class="control-label" for="paranix_comune">Comune</label>
        <div class="controls">
            <input type="hidden" id="paranix_comune_id" name="paranix_comune_id" class="hidden" />
            <input type="text" class="form-control" id="paranix_comune" name="paranix_comune" value="<?= $comune[0]->name; ?>"/>
        </div>
    </div>
    
    <?php if($scuola instanceof WP_Post): ?>
    <div class="col-lg-12">
        <label class="control-label" for="paranix_childname">Scuola frequentata</label>
        <div class="controls">
            <input type="hidden" name="paranix_school_registrazione" value="1" class="hide" />
            <select name="paranix_user_school" id="paranix_user_school"  class="form-control"
                    data-validation="required" 
                    data-validation-error-msg="Il campo scuola del bambino non può essere vuoto"
                    placeholder="Scuola frequentata"
                    >
                <option>Scegli scuola</option>
            </select>
            <input type="text" class="form-control" id="paranix_school_registrazione" name="paranix_school_registrazione" value="<?= $scuola->post_title; ?>"/>
        </div>
    </div>
    <div class="col-lg-12">
        <label class="control-label" for="paranix_user_school_room">Aula</label>
        <div class="controls">
            <input type="text" class="form-control" id="paranix_user_school_room" name="paranix_user_school_room" value="<?= $aula; ?>"/>
        </div>
    </div>
    <?php endif; ?>
    
    <div class="col-lg-12">&nbsp;</div>
    <div class="pull-right">
        <button type="submit" class="btn btn-primary">Aggiorna</button>
    </div>
</form>