<?php global $already_nav; ?>
<nav class="navbar navbar-default background_purple topnavigation" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navigation-header">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navigation-header" >
            <?php wp_nav_menu(Paranix_Core_LayoutManager::header_menu_configuration()); ?>

            <div class="socialnav pull-right">
                <?php Paranix_Core_LayoutManager::include_partials('user-header.php'); ?>
                <?php if ($_SESSION['already_nav'] == 1): ?>
                    <div class="text-center">
                        <a href="/policyprivacy/" class="text-white" style="font-size:13px;">
                            Privacy Policy
                        </a>
                    </div>
                <?php $_SESSION['already_nav'] = 0; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</nav>