<?php 
/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
global $home_configuration;
$box_app = $home_configuration->get_main_post();
$box_news = $home_configuration->get_news();
?>
<div class="col-md-12" id="projecthomepage">
    <div class="col-md-8 col-mabottom">
        <h3 class="homepage"><?= $box_app['box_title']; ?></h3>
        <div class="col-md-12">
            <p>
                <img src="<?= $box_app['post_image']; ?>" class="pull-left"/>
                <?= $box_app['post_excerpt']; ?>&nbsp;<a class="morelink" href="<?= $box_app['post_link']; ?>">per saperne di più</a>
            </p>
            
            <a class="morelink" href="/area-insegnanti/">Area insegnanti</a>
            
            <p class="col-md-6 pull-right" style="margin-top:20px;">
                <?php foreach($box_app['box_links'] as $link): ?>
                <a href="<?= $link['paranix_box_main_post_links_link']; ?>" class="ctalink"><?= $link['title']; ?></a>
                <?php endforeach; ?>
            </p>
            <hr/>
        </div>
    </div>
    <div class="col-md-4 col-mabottom">
        <div class="col-md-11 col-md-offset-1 roundedbox">
            <h2 class="text-muted text-center"><strong><?= $box_news['box_title']; ?></strong></h2>
            <ul id="pidonewslist" class="text-left">
                <?php foreach ($box_news['posts'] as $news): ?>
                <li><a href="<?= get_permalink( $news->ID ); ?>"><?= $news->post_title; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <p class="text-center">
                <a href="/archivio/?category=pido-news" class="ctalink">Tutte le news!</a>
            </p>
        </div>
    </div>
</div>

