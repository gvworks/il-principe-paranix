<?php
    global $home_configuration;
    $most_liked = Paranix_Core_PhotoContest::get_most_liked();
    
    $most_liked_url = esc_url(get_permalink(get_page_by_title('Concorso')));
    $most_liked_photo = '/uploads/default-contest.png';
    
    if(count($most_liked) > 0){
        $most_liked_url = $most_liked[0]['photo_url'];
        $most_liked_photo = $most_liked[0]['photo_image_url'];
    }
    
    $box_app = $home_configuration->get_app_box();
?>
<div class="col-md-12">
    <h3 class="homepage">Il concorso!</h3>
    <div class="col-sm-6 col-md-4 text-center col-mabottom">
        <h3 class="text-muted"><strong>La foto più votata!</strong></h3>
        <figure class="frame-orange">
            <a href="<?= $most_liked_url; ?>">
                <img src="<?= $most_liked_photo; ?>" class="img-responsive"/>
            </a>
        </figure>
        <a href="<?= esc_url(get_permalink(get_page_by_title('Concorso'))); ?>" class="btn btn-camera-custom">
            <span>Vedi tutte le foto</span>
        </a>
    </div>
    <div class="col-sm-6 col-md-5 col-md-offset-2">
        <div class="col-md-12">
            <div id="apphomepage" class="boxshadow boxrounded text-center">
                <img height="200" src="<?= $box_app['box_image']; ?>" />
                <p class="text-center">
                    <?= $box_app['post_content']; ?>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">&nbsp;</div>