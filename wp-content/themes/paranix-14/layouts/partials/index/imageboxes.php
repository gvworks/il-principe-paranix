<?php 
/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
global $home_configuration;
$box_images = $home_configuration->get_image_boxes();
?>
<div class="col-lg-12">
    <?php foreach($box_images as $box_image): ?>
    <div class="col-xs-12 col-md-6">
        <div class="col-sm-10 col-sm-offset-1 col-md-12 col-md-offset-0 boxshadow boxrounded col-mabottom">
            <h3 class="homepage"><?= $box_image['title']; ?></h3>
            <p class="text-center">
                <?php
                    $attachment_id = Paranix_Core_MediaManager::get_attachment_id_by_url($box_image['paranix_home_images_item_image']);
                    $thumbnail = wp_get_attachment_image_src( $attachment_id, array(480,363));
                ?>
                <img src="<?= $thumbnail[0]; ?>" class="thumbnail img-responsive" alt="<?= $box_image['title']; ?>" />
                <a href="<?= $box_image['paranix_home_images_item_button_link']; ?>" class="ctalink">
                    <?= $box_image['paranix_home_images_item_button_label']; ?>
                </a>
            </p>
        </div>
    </div>
    <?php endforeach; ?>
</div>