<nav class="navbar  background_green row" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#product-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="navbar-brand">Prodotti</span>
        </div>
        <?php wp_nav_menu(Paranix_Core_LayoutManager::product_menu_configuration()); ?>
    </div>
</nav>