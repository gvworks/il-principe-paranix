<?php
/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'default-small-cropped');
?>

<div class="col-md-12">
    <div class="col-xs-12 col-sm-3 col-md-3 text-center">
        <img src="<?= $featured_image[0]; ?>" height="280" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"/>
    </div>
    <span class="col-xs-12 col-sm-12 col-md-9">
        <h3 class="bigh3title"><?php the_title(); ?></h3>
        <span class="general-text">
           <?php the_excerpt(); ?>
        </span>
    </span>
</div>
<div class="col-md-12">&nbsp;</div>
<?php the_content(); ?>
<div class="col-md-12">&nbsp;</div>
