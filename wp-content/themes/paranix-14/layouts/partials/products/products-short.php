<?php
/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id( $product_post->ID ), 'default-small-cropped');
?>
<div class="col-md-12 boxrounded boxshadow col-mabottom padding-top-bottom">
    <div class="col-xs-12 col-sm-3 col-md-3 text-center">
        <img src="<?= $featured_image[0]; ?>" class="img-responsive" alt="<?= $product_post->post_title; ?>" title="<?= $product_post->post_title; ?>" />
    </div>
    <span class="col-xs-12 col-sm-9 col-md-9">
        <h3 class="bigh3title text_<?= $color_title?>"><?= $product_post->post_title; ?></h3>
        <span class="general-text">
            <?= $product_post->post_excerpt; ?>
        </span>
    </span>
</div>
<div class="col-md-12">&nbsp;</div>