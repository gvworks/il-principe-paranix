<div class="col-md-12 jumbotron ohidden">
    <div class="col-md-3">
        <a href="#" class="thumbnail gallery-video active">
            <img src="http://img.youtube.com/vi/hdXwCOcsNBo/0.jpg" 
                 alt='Guarda la storia animata “Le avventure del PrincipeParanix” liberamente  tratta dall’elaborato artistico della classe IV della scuola “A. Barni” di Dovera (CR), vincitore della prima edizione del progetto didattico' 
                 data-target="//www.youtube.com/embed/hdXwCOcsNBo" />
        </a>
        <a href="#" class="thumbnail gallery-video">
            <img src="http://img.youtube.com/vi/FZ_uUyCszKM/0.jpg" 
                 alt="Scopri il fumetto animato Prince Paranix and the invasion of ultra-Lice e impara l’Inglese con il principe Paranix!" 
                 data-target="//www.youtube.com/embed/FZ_uUyCszKM" />
        </a>
    </div>
    <div class="col-md-9">
        <div class="caption">
            <h3 class="text-success">Guarda la storia animata “Le avventure del PrincipeParanix” liberamente  tratta dall’elaborato artistico della classe IV della scuola “A. Barni” di Dovera (CR), vincitore della prima edizione del progetto didattico</h3>
        </div>
        <hr/>
        <div class="embed-responsive embed-responsive-4by3">
            <iframe class="embed-responsive-item" src="//www.youtube.com/embed/hdXwCOcsNBo"></iframe>
        </div>
    </div>
</div>


<div class="col-md-12 jumbotron ohidden">
    <div class="page-header text-center">
        <h3><strong>Parla l’esperto – Il Professor Scacciapidocchi</strong></h3>
    </div>
    <div class="col-md-3">
        <a href="#" class="thumbnail gallery-video active">
            <img src="http://img.youtube.com/vi/U8aEbuevpeo/0.jpg" 
                 alt="Chi è il pidocchio?" 
                 data-target="//www.youtube.com/embed/U8aEbuevpeo" />
        </a>
        <a href="#" class="thumbnail gallery-video">
            <img src="http://img.youtube.com/vi/NVU4A4_5UBA/0.jpg" 
                 alt="Come individuare i pidocchi?" 
                 data-target="//www.youtube.com/embed/NVU4A4_5UBA" />
        </a>
        
        <a href="#" class="thumbnail gallery-video active">
            <img src="http://img.youtube.com/vi/bnLhwHsd2vk/0.jpg" 
                 alt="Quali sono gli effetti causati dalla presenza dei pidocchi?" 
                 data-target="//www.youtube.com/embed/bnLhwHsd2vk" />
        </a>
        <a href="#" class="thumbnail gallery-video">
            <img src="http://img.youtube.com/vi/VVWQZWmFIXU/0.jpg" 
                 alt="Come si trasmettono i pidocchi?" 
                 data-target="//www.youtube.com/embed/VVWQZWmFIXU" />
        </a>
        
        <a href="#" class="thumbnail gallery-video active">
            <img src="http://img.youtube.com/vi/fHgQmsq8d5c/0.jpg" 
                 alt="Cosa fare quando si scopre di avere i pidocchi?" 
                 data-target="//www.youtube.com/embed/fHgQmsq8d5c" />
        </a>
        <a href="#" class="thumbnail gallery-video">
            <img src="http://img.youtube.com/vi/N3BjMYZKilU/0.jpg" 
                 alt="I metodi della nonna e le curiosità sui pidocchi" 
                 data-target="//www.youtube.com/embed/N3BjMYZKilU" />
        </a>
        
        <a href="#" class="thumbnail gallery-video active">
            <img src="http://img.youtube.com/vi/Dw2UZk0JT1U/0.jpg" 
                 alt="Come si sconfiggono i pidocchi?" 
                 data-target="//www.youtube.com/embed/Dw2UZk0JT1U" />
        </a>
        <a href="#" class="thumbnail gallery-video">
            <img src="http://img.youtube.com/vi/XJnziBbeDek/0.jpg" 
                 alt="Si possono prevenire i pidocchi?" 
                 data-target="//www.youtube.com/embed/XJnziBbeDek" />
        </a>
        
    </div>
    <div class="col-md-9">
        <div class="caption">
            <h3 class="text-success">Chi è il pidocchio?</h3>
        </div>
        <hr/>
        <div class="embed-responsive embed-responsive-4by3">
            <iframe class="embed-responsive-item" src="//www.youtube.com/embed/U8aEbuevpeo"></iframe>
        </div>
    </div>
</div>