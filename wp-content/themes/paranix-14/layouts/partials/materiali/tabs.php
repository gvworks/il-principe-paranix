<h3 class="homepage">Materiali</h3>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 bhoechie-tab-menu">
        <div class="list-group">
            <a href="#" class="list-group-item active text-center">
                <h4 class="glyphicon glyphicon-download-alt"></h4><br/>Kit Educativo
            </a>
            <a href="#" class="list-group-item text-center">
                <h4 class="glyphicon glyphicon-zoom-in"></h4><br/>Identikit del Pidocchio
            </a>
            <a href="#" class="list-group-item text-center">
                <h4 class="glyphicon glyphicon-question-sign"></h4><br/>Lo sapevate che?
            </a>
            <a href="#" class="list-group-item text-center">
                <h4 class="glyphicon glyphicon-eye-open"></h4><br/>Occhio al Pidocchio
            </a>
            <a href="#" class="list-group-item text-center">
                <h4 class="glyphicon glyphicon-facetime-video"></h4><br/>Video
            </a>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 bhoechie-tab">
        <div class="bhoechie-tab-content active">
            <?php
            $post_kit_educativo = get_page_by_title('Kit Educativo', OBJECT, 'post');
            ?>
            <h3 class="bigh3title"><?= $post_kit_educativo->post_title; ?></h3>
            <?= apply_filters('the_content', $post_kit_educativo->post_content); ?>
            <p>&nbsp;</p>
        </div>
        <div class="bhoechie-tab-content">
            
            <?php
            $post_identikit = get_page_by_title('Identikit del pidocchio', OBJECT, 'post');
            ?>
            <h3 class="bigh3title"><?= $post_identikit->post_title; ?></h3>
            <?= apply_filters('the_content', $post_identikit->post_content); ?>
            <p>&nbsp;</p>
        </div>
        <div class="bhoechie-tab-content">
            <?php
            $post_lo_sapevate_che = get_page_by_title('Lo sapevate che?', OBJECT, 'page');
            ?>
            <h3 class="bigh3title"><?= $post_lo_sapevate_che->post_title; ?></h3>
            <?= apply_filters('the_content', $post_lo_sapevate_che->post_content); ?>
            <p>&nbsp;</p>
        </div>
        <div class="bhoechie-tab-content">
            <?php
            $post_occhio_al_pidocchio = get_page_by_title('Occhio al pidocchio!', OBJECT, 'page');
            ?>
            <h3 class="bigh3title"><?= $post_occhio_al_pidocchio->post_title; ?></h3>
            <?= apply_filters('the_content', $post_occhio_al_pidocchio->post_content); ?>
            <p>&nbsp;</p>
        </div>
        <div class="bhoechie-tab-content">
            <?php Paranix_Core_LayoutManager::include_partials('video.php', 'materiali'); ?>
            <p>&nbsp;</p>
        </div>
    </div>
</div>