<div class="well">
    <h3 class="bigh3title text_purple">Carica la tua foto!</h3>
    <hr/>
    <div class="media">
        <section class="pull-left">
            <img id="paranix_user_preview_image" class="media-object" src="<?= PARANIX_ASSETS_FOLDER?>/custom/images/avatar.jpg" alt="Carica una foto" />
            <br/>
            <input type="file" id="paranix_userimage" name="paranix_userimage"/>
            <?php wp_nonce_field('my_image_upload', 'my_image_upload_nonce'); ?>
        </section>
        <div class="media-body">
            <h4 class="media-heading">
                Se non hai adesso la foto, puoi caricarla in un secondo momento. <br/>
                Puoi anche scaricare la App <strong>"Principe Paranix"</strong> e caricare la foto dal tuo cellulare.
            </h4>
            <small>Una volta registrato puoi caricare altre foto!</small>
            <div class="control-group">
                <label class="control-label" for="paranix_photo_description">Descrizione della foto</label>
                <div class="controls">
                    <textarea id="paranix_photo_description"  name="paranix_photo_description" class="form-control" rows=7"></textarea><br/>
                </div>
            </div>

        </div>
    </div>
</div>