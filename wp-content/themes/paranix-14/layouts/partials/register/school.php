<hr/>

<h3 class="bigh3title text_purple">Iscriviti!</h3>
<!-- Text input-->

<div class="row">
    <div class="col-lg-6">
        <label class="control-label" for="paranix_userfirstname">Nome <span class="danger">*</span></label>
        <input type="text" class="form-control" id="paranix_userfirstname" name="paranix_userfirstname" 
               data-validation="required" 
               data-validation-error-msg="Il campo nome non può essere vuoto"
               placeholder="Nome"/>
    </div>
    <div class="col-lg-6">
        <label class="control-label" for="paranix_userlastname">Cognome <span class="danger">*</span></label>
        <input type="text" class="form-control" id="paranix_userlastname" name="paranix_userlastname"
               data-validation="required" 
               data-validation-error-msg="Il campo cognome non può essere vuoto"
               placeholder="Cognome"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="control-group">
            <label class="control-label" for="paranix_childname">Nome del bambino <span class="danger">*</span></label>
            <input type="text" class="form-control" id="paranix_childname" name="paranix_childname" 
                   data-validation="required" 
                   data-validation-error-msg="Il campo nome del bambino non può essere vuoto"
                   placeholder="Nome del bambino" />
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <label class="control-label" for="paranix_useremail">Email <span class="danger">*</span></label>
        <div class="controls">
            <input type="email" class="form-control" id="paranix_useremail" name="paranix_useremail" 
                   data-validation="email" 
                   accept=""data-validation-error-msg="Il campo email non può essere vuoto"
                   placeholder="email@ilmioindirizzo.it" />
        </div>
    </div>
    <div class="col-lg-6">
        <label class="control-label" for="paranix_userpassword">Password <span class="danger">*</span></label>
        <input type="password" class="form-control" id="paranix_userpassword" name="paranix_userpassword" 
               data-validation="required" 
               data-validation-error-msg="Il campo password non può essere vuoto"
               placeholder="Password"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <label class="control-label" for="paranix_usertel">Telefono <span class="danger">*</span></label>
        <input type="tel" class="form-control" id="paranix_usertel" name="paranix_usertel" 
               data-validation="number" 
               data-validation-error-msg="Il campo telefono non può essere vuoto"
               placeholder="Telefono" />
    </div>
    <div class="col-lg-6">
        <label class="control-label" for="paranix_comune">Comune di residenza <span class="danger">*</span></label>
        <input type="text" id="paranix_comune" name="paranix_comune" class="form-control" 
               data-validation="required" 
               data-validation-error-msg="Il campo comune non può essere vuoto"
               placeholder="Comune di residenza"/>
        <input type="hidden" id="paranix_comune_id" name="paranix_comune_id" class="hidden" />
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="control-group">
            <label class="control-label" for="paranix_user_school">Scuola frequentata <span class="danger">*</span></label>
            <input type="hidden" name="paranix_school_registrazione" value="1" class="hide" />
            <select name="paranix_user_school" id="paranix_user_school"  class="form-control"
                    data-validation="required" 
                    data-validation-error-msg="Il campo scuola del bambino non può essere vuoto"
                    placeholder="Scuola frequentata"
                    >
                <option>Scegli scuola</option>
            </select>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="control-group">
            <label class="control-label" for="paranix_user_school_room">Classe <span class="danger">*</span></label>
            <select name="paranix_user_school_room" id="paranix_user_school_room"  class="form-control"
                    data-validation="required" 
                    data-validation-error-msg="Il campo scuola del bambino non può essere vuoto"
                    placeholder="Scuola frequentata"
                    >
                <option>Scegli classe</option>
            </select>
        </div>
    </div>
</div>

<div class="row">&nbsp;</div>
<div class="row">
    <div class="col-lg-12">
        <label class="control-label" for="paranix_clause">Accetta le clausole di partecipazione incluse nel regolamento di concorso? <span class="danger">*</span></label>
        <input type="checkbox" class="input" id="paranix_clause" name="paranix_clause" 
               data-validation="required" 
               data-validation-error-msg="Devi accettare le clausole di partecipazione."
               />
    </div>
    <hr/>
    <div class="col-lg-6">
        <textarea class="form-control" disabled><?= Paranix_PageManager_Global::get_regolamento_concorso(); ?>
        </textarea>
        <br/>
        <label class="control-label" for="paranix_informativa_clause">Accetto<span class="danger">*</span></label>
        <input type="checkbox" class="input" id="paranix_informativa_clause" name="paranix_informativa_clause" 
               data-validation="required" 
               data-validation-error-msg="Devi esprimere il consenso al trattamento dei dati."
               />
    </div>
    <div class="col-lg-6">
        <textarea class="form-control" disabled><?= Paranix_PageManager_Global::get_utilizzo_dati(); ?></textarea>
        <br/>
        
        <label class="control-label" for="paranix_chefaro_clause">Accetto<span class="danger">*</span></label>
        <input type="checkbox" class="input" id="paranix_chefaro_clause" name="paranix_chefaro_clause" 
               data-validation="required" 
               data-validation-error-msg="Il campo nome non può essere vuoto"
               />
    </div>
</div>
<div class="col-md-12">
    * I campi contrassegnati con l'asterisco sono obbligatori.
</div>
<div class="row">&nbsp;</div>
<div class="row">
    <div class="col-lg-12">
        <button class="btn btn-success pull-right">Iscriviti</button>
    </div>
</div>