<?php global $posts; ?>
<?php foreach ($posts as $post): ?>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <div class="row">
                <div class="col-md-12">
                    <h4><strong><a href="<?= get_post_permalink($post->ID, true); ?>"><?= $post->post_title; ?></a></strong></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="<?= get_post_permalink($post->ID, true); ?>" class="thumbnail">
                        <img src="http://www.ilprincipeparanix.it/uploads/default-contest.png" class="img-responsive">
                    </a>
                </div>
                <div class="col-md-8">      
                    <p class="general-text"><?= $post->post_excerpt; ?></p>
                    <div class="row">
                        <div class="col-md-12">
                            <small>
                                <i class="icon-user"></i> Inserito da <?= get_the_author_meta( 'first_name', $post->post_author );?> 
                                | <i class="icon-calendar"></i> <?= get_the_date('d/m/Y \a\l\l\e g:ia', $post->ID); ?>
                            </small>
                        </div>
                    </div>
                    <hr/>
                    <p>
                        <a class="btn btn-success" href="<?= get_post_permalink($post->ID, true); ?>">
                            <i class="glyphicon glyphicon-file"></i> Continua a leggere!
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <hr>
<?php endforeach; ?>