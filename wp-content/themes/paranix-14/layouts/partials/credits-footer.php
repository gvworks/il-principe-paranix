<?php $credits_footer = Paranix_PageManager_Global::get_credits_footer(); ?>
<div class="col-md-12">
    <?php foreach($credits_footer as $index => $credit): ?>
    <div class="col-zs-12 col-sm-12 col-md-6">
        <p class="<?= ($index % 2 == 0) ? 'text-left' : 'text-right'; ?>">
            <?= $credit['title']; ?> &nbsp;
            <a href="<?= $credit['paranix_box_credits_image_link']; ?>">
                <img src="<?= $credit['paranix_box_credits_image']; ?>"  height="40"/>
            </a>
        </p>
    </div>
    <?php endforeach; ?>
    <!--<div class="col-zs-12 col-sm-12 col-md-6">
        <p class="text-right">
            Progetto didattico promosso da &nbsp;
            <img src="/assets/custom/images/omega-che-faro.png"  height="40"/>
        </p>
    </div>-->
</div>