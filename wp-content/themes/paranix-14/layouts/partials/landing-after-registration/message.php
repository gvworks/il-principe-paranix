<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

?>

<div class="media">
  <a class="pull-left" href="#">
      <img class="media-object" src="/assets/custom/images/paranix.png" />
  </a>
  <div class="media-body">
      <h3 class="media-heading text-white">
          <strong>Registrazione effettuata con successo!</strong>
      </h3>
    <hr/>
    <p class="text-white general-text">
        Ciao <?= Paranix_Core_UserManager::get_temporary_stored_name(); ?>, la registrazione è andata a buon fine. <br/>Ti manca solo un ultimo passo.<br/>
        Ti è stata spedita una email all'indirizzo con cui ti sei registrato, clicca sul link che trovi nella mail per confermare la tua registrazione.<br/>
        <br/>
        <b>Il Principe Paranix</b>
    </p>
    <hr/>
    <p>
        <a class="btn btn-warning" href="<?= get_home_url(); ?>">
            <i class="glyphicon glyphicon-home"></i> Torna alla home del sito
        </a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-success" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Le foto del contest' ) ) ); ?>">
            <i class="glyphicon glyphicon-camera"></i> Vedi le foto del contest
        </a>
    </p>
  </div>
</div>