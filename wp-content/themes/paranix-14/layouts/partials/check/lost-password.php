<?php 
    $form_post_url = Paranix_Core_LoaderManager::get_page_url_by_title('Utenti - Funzioni speciali', array('action' => 'recoverpassword'));
?>

<h3 class="mediumtitle text-center">Inserisci l'indirizzo email con cui ti sei registrato!</h3>

<div class="row">
    <div class="col-lg-12 text-left">
        <button class="btn btn-warning" id="lost-password-back">
            <i class="glyphicon glyphicon-arrow-left"></i> Torna indietro
        </button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">&nbsp;</div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <form method="POST" action="<?= $form_post_url; ?>" name="paranix_lost_password">
            <div class="input-group">
                <input type="email" class="form-control" id="paranix_login_email" name="paranix_login_email" placeholder="Indirizzo email"/>
                <input type="hidden" class="hide" id="requestaction" name="requestaction" value="recoverpassword"/>
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-success">Recupera password</button>
                </span>
            </div>
        </form>
    </div>
</div>
