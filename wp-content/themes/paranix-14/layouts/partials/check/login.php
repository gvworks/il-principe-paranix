<?php

$url_to_login = Paranix_Core_LoaderManager::get_page_url_by_title('Utenti - Funzioni speciali', array('action' => 'login'));

?>

<form accept-charset="UTF-8" action="<?= $url_to_login; ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">

    <h3 class="mediumtitle text-center">Sei già registrato? <br/>Effettua il login!</h3>

    <div class="row">
        
        <?php
        if (Paranix_Core_FlashMessage::has_message()) {
            print Paranix_Core_FlashMessage::return_formatted_message();
        }
        ?>
        
        <div class="col-lg-12">
            <label class="control-label text-white" for="username">Email</label>
            <div class="controls">
                <input type="email" class="form-control" id="username" name="paranix_login_email" />
            </div>
        </div>
        <div class="col-lg-12">
            <label class="control-label text-white" for="password">Password</label>
            <div class="controls">
                <input type="password" class="form-control" id="password" name="paranix_user_password" />
            </div>
        </div>
    </div>
    <input type="hidden" class="hide" id="requestaction" name="requestaction" value="login"/>
    <div class="row">&nbsp;</div>

    <div class="row">
        <div class="col-sm-12">
            <div class="controls">
                <button class="btn btn-success pull-right">Login</button>
            </div>
        </div>
        <div class="col-sm-12">&nbsp;</div>
        <div class="col-sm-12 text-center text_purple">
            <a href="#" class="text-white" id='lost-password-button'>Hai dimenticato la password?</a>
        </div>
    </div>

</form>