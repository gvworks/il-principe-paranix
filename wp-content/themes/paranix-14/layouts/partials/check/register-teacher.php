<h3 class="mediumtitle text-center">Non sei registrato? Registrati!</h3>

<div class="row">
    <div class="col-lg-12 text-center">
        <a href="">
            <img src="<?= PARANIX_ASSETS_FOLDER . '/custom/images/paranix.png'; ?>"  width="200"/>
        </a>
        <a href="<?= Paranix_Core_LoaderManager::get_page_url_by_title('Registrazione docenti'); ?>" class="btn btn-warning">
            <i class="glyphicon glyphicon-share-alt"></i> Registrati!
        </a>
    </div>
</div>
