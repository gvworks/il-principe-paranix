<h3 class="mediumtitle text-center">Chi sei?</h3>

<div class="row">
    <div class="col-lg-12 text-left">
        <button class="btn btn-warning" id="choose-registration-type-button-back">
            <i class="glyphicon glyphicon-arrow-left"></i> Torna indietro
        </button>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">&nbsp;</div>
</div>

<div class="row">
    <div class="col-xs-6 col-sm-6 col-lg-6  text-center">
        <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Registrazione utente web' ) ) ); ?>" class="text-center">
            <img class="img-responsive" src="<?= PARANIX_ASSETS_FOLDER; ?>/custom/images/paranix-registration-web.png" />
        </a>
        <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Registrazione utente web' ) ) ); ?>" class="text-white mediumtitle">Sono un visitatore del sito</a>
    </div>
     <div class="col-xs-6 col-sm-6 col-lg-6 text-center">
         <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Registrazione utente scuola' ) ) ); ?>" class="text-center">
            <img class="img-responsive" src="<?= PARANIX_ASSETS_FOLDER; ?>/custom/images/paranix-registration-edu.png" />
        </a>
        <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'Registrazione utente scuola' ) ) ); ?>" class="text-white mediumtitle">Mio figlio partecipa al progetto didattico</a>
    </div>
</div>
