<?php
$paranix_slides = Paranix_Core_SliderManager::get();
$paranix_total_slides = count($paranix_slides);
?>
<div id="paranix-slider" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php for ($i = 0; $i < $paranix_total_slides; ++$i): $paranix_slider_active_class = ($i == 0) ? 'active' : ''; ?>
            <li data-target="#paranix-slider" data-slide-to="<?= $i; ?>" class="<?= $paranix_slider_active_class; ?>"></li>
        <?php endfor; ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php foreach($paranix_slides as $paranix_slider_conter => $paranix_slide): $paranix_slider_active_class = ($paranix_slider_conter == 0) ? 'active' : ''; ?>
        <div class="item <?= $paranix_slider_active_class; ?>">
            <a href="<?= $paranix_slide['paranix_slide_url']; ?>">
                <img src="<?= $paranix_slide['paranix_slide_image']; ?>" alt="...">
            </a>
        </div>
        <?php endforeach; ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#paranix-slider" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#paranix-slider" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
