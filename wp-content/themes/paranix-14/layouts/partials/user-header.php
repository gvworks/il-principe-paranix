<?php
    if(!is_user_logged_in()){
        $button_color = 'btn-primary';
        $button_url = Paranix_Core_LoaderManager::get_page_url_by_title('Sign up');
        $button_primary_text = 'Effettua il login';
        $button_secondary_text = 'oppure registrati!';
        $button_icon = 'glyphicon glyphicon-off';
    }
    else{
        global $current_user;
        get_currentuserinfo();
        $button_color = 'btn-success';
        $button_url = Paranix_Core_LoaderManager::get_page_url_by_title('Area utente');
        $button_primary_text = 'Ciao '.$current_user->user_firstname;
        $button_secondary_text = 'vai all\'area utente!';
        $button_icon = 'glyphicon glyphicon-user';
    }
?>


<a class="btn btn-lg <?= $button_color; ?>" href="<?= $button_url; ?>">
    <i class="<?= $button_icon; ?> pull-left"></i><span>&nbsp;<?= $button_primary_text; ?><br><small><?= $button_secondary_text; ?></small></span>
</a> 