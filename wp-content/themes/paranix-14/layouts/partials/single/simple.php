<div class="container-fluid">
    <div class="col-sm-12 castle-before">
        <?php Paranix_Core_LayoutManager::include_partials('svg-castle.php', 'check'); ?>
    </div>
    <div class="col-sm-12 padding-top-bottom castle-after col-mabottom">
        <div class="col-md-12 bordered-top background_white">
            <h3 class="bigh3title"><?php the_title(); ?></h3>
            <p>
                <?php the_content(); ?>
            </p>
        </div>
    </div>
</div>
<hr/>