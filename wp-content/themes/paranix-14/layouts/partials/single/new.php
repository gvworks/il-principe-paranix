<?php $class = ($_SERVER['REQUEST_URI'] == '/premi/') ? '' : 'castle-bottom'; ?>
<div class="col-md-12 <?= $class; ?>">
    <h3 class="homepage"><?php the_title(); ?></h3>
    <p>
        <?php the_content(); ?>
    </p>
</div>