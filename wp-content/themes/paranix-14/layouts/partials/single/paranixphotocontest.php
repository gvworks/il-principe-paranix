<?php
global $post;
$featured_image = wp_get_attachment_image_src(
        get_post_thumbnail_id(get_the_ID()), 'contest-list-cropped'
);
$childname = get_post_meta(get_the_ID(), Paranix_Core_PhotoContest::CHILDNAME_VAR, TRUE);
$photo_likes = get_post_meta(get_the_ID(), Paranix_Core_PhotoContest::LIKES_VAR, TRUE);

$school_value = Paranix_Core_PhotoContest::get_photo_school(get_the_ID());

?>
<div class="media">
    <a class="pull-left">
        <img class="media-object img-thumbnail" src="<?= $featured_image[0]; ?>" alt="<?= $childname; ?>" width="300" />
    </a>
    <div class="media-body">
        <h2 class="media-heading text_purple"><strong><?= $childname; ?></strong></h2>
        <p>Inserita il: <span class="date sub-text" id="photodate"><?= get_the_date('d/m/Y \a\l\l\e g:ia');?></span></p>
        <p>
            <i class="glyphicon glyphicon-star  text_purple"></i>Voti totali: <?= $photo_likes; ?>
            <div class="fb-like" data-href="<?= get_the_permalink(); ?>" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
        </p>
        <p class="lead text-muted"><?= get_the_excerpt(); ?></p>
        <?php if(count($school_value)): ?>
        <span class="btn btn-lg btn-primary">
          <i class="glyphicon glyphicon-briefcase pull-left"></i><span>Scuola<br><small><?= $school_value['school']; ?></small></span>
        </span>
        <?php endif; ?>
    </div>
</div>