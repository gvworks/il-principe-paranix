<div id="social_lightbox" class="modal img-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-md-8 modal-image">
                        <img id="social_lightbox_image" class="img-responsive " src="">
                        <a href="" class="img-modal-btn left social_lightbox_direction" data-direction="left">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                        <a href="" class="img-modal-btn right social_lightbox_direction" data-direction="right">
                            <i class="glyphicon glyphicon-chevron-right"></i>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-4 modal-meta">
                        <div class="modal-meta-top">
                            <button type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <ul class="img-comment-list">
                                <li>
                                    <div class="comment-img">
                                        <img id="useravatar" src="">
                                    </div>
                                    <div class="comment-text">
                                        <strong class="text-primary" id="avatarusername"></strong>
                                        <p>Inserita il: <span class="date sub-text" id="photodate"></span></p>
                                    </div>
                                </li>
                            </ul>
                            <div class="img-poster clearfix">
                                <div class="media-body" id="description">
                                </div>
                                <p>&nbsp;</p>
                                <p class="middle">Voti totali:&nbsp;<strong class="text_green" id="social_lightbox_likes"></strong>&nbsp;<i class="glyphicon glyphicon-star  text_purple"></i></p>
                                <p>&nbsp;</p>
                                <div id="fblike" class="fb-like pull-right" data-href="" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>