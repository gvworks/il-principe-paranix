<?php global $photo_contest_list, $sorting_likes_button, $sorting_date_button; ?>
<h3 class="bigh3title">Tutte le foto</h3>
<div class="well well-sm col-md-12 ohidden">
    <div class="pull-right">
        <strong>Ordina per</strong>
        <div class="btn-group">
            <a href="<?= $sorting_likes_button; ?>" id="list" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-thumbs-up"></span>
                <strong>Più votata</strong>
            </a>
            <a href="<?= $sorting_date_button; ?>" id="grid" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-sort-by-order"></span> <strong>Data di inserimento</strong>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div id="gallery-container" class="col-lg-12">
        <?php foreach ($photo_contest_list as $photo): ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="col-lg-10 col-lg-offset-1 polaroid-effect">
                    <a 
                        data-image="<?= $photo['photo_image_url_full_size']; ?>" 
                        data-title="<?= $photo['photo_childname']; ?>" 
                        data-description="<?= $photo['photo_description']; ?>" 
                        data-href="<?= $photo['photo_url']; ?>" 
                        data-likes="<?= $photo['photo_likes']; ?>"
                        data-insertdate="<?= $photo['photo_date']; ?>"
                        data-userimage="<?= $photo['photo_useravatar']; ?>"
                        data-username="<?= $photo['photo_username']; ?>"
                        class="gallery-items">
                        <img src="<?= $photo['photo_image_url']; ?>" alt="<?= $photo['photo_title']; ?>" class="img-responsive border-bottom-gray">
                    </a>
                    <div class="col-lg-12">&nbsp;</div>
                    <div class="col-lg-12 border-bottom-gray">
                        <p><strong class="text_green"><?= $photo['photo_childname']; ?></strong></p>
                    </div>
                    <div class="col-lg-12 border-bottom-gray">
                        <p class="middle">Voti totali:&nbsp;<strong class="text_green"><?= $photo['photo_likes']; ?></strong>&nbsp;<i class="glyphicon glyphicon-star text_purple"></i></p>
                    </div>
                    <div class="col-lg-12">&nbsp;</div>
                    <div class="col-lg-12 text-center">
                        <strong class="text_purple">Metti mi piace e vota questa foto!</strong><br/><br/>
                        <div class="fb-like" data-href="<?= $photo['photo_url']; ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                    </div>
                    <div class="col-lg-12">&nbsp;</div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>