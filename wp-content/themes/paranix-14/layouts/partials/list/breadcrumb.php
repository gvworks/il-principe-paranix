<nav class="navbar  background_green row" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#contest-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <?php wp_nav_menu(Paranix_Core_LayoutManager::contest_menu_configuration()); ?>
    </div><!-- /.container-fluid -->
</nav>
