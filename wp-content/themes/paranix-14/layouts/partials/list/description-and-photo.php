<?php

global $photo_contest;

$paranix_description_contest_post_config = array(
    'posts_per_page' => 1,
    'offset' => 0,
    'category' => get_cat_ID('Descrizione concorso'),
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true
);

$paranix_description_contest_post = get_posts($paranix_description_contest_post_config);

$top_of_the_week = $photo_contest->get_photo_of_the_week(3);

?>

<div class="col-md-12">
    <div class="col-xs-12 col-md-7">
        <h3 class='text-center h3gray'>It's PidoTime!</h3>
        <h4 class='text-center h4gray'>Le foto più pidocchiose della settimana!</h4>
        <hr/>
        <?php foreach($top_of_the_week as $index => $tofw): ?>
        <?php
        if($index == 0){
            $col = '5';
            $pido = 'big';
            $border = 'green';
        }
        elseif($index == 1){
            $col = '4';
            $pido = 'medium';
            $border = 'purple';
        }
        else{
            $col = '3';
            $pido = 'small';
            $border = 'orange';
        }
        ?>
        <div class="col-sm-<?= $col; ?> col-md-<?= $col; ?>">
            <a href="<?= $tofw['photo_url']; ?>">
                <img class="img-circle img-responsive border_image <?= $border; ?>" src="<?= $tofw['image']; ?>" />
            </a>
            <p class="text-center">
                <img src="/assets/custom/images/pido-<?= $pido; ?>.png" />
            </p>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="col-xs-12 col-md-5">
        <h3 class="homepage"><?= $paranix_description_contest_post[0]->post_title; ?></h3>
        <div class="media">
            <a class="col-xs-12 col-sm-12 col-md-4 text-center" href="#">
                <img src="/assets/custom/images/bambini.png" class="img-responsive" />
            </a>
            <div class="col-xs-12 col-sm-12 col-md-8">
                <?= $paranix_description_contest_post[0]->post_content; ?>
                <p>
                    <a class="btn btn-success" href="<?= Paranix_Core_LoaderManager::get_page_url_by_title('Sign up'); ?>">
                        <i class="glyphicon glyphicon-ok"></i> Partecipa!
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>