<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_Core_MediaManager {

    private static $upload_dir_articoli = '/articoli';
    private static $upload_dir_contest = '/contest';
    private static $upload_dir_pagine = '/pagine';
    private static $upload_dir_scuole = '/scuole';
    private static $upload_dir_slides = '/slides';

    public static function upload_manager($folder) {
        global $custom_post_type;

        if (isset($custom_post_type)) {
            $post_type = $custom_post_type;
        } else {
            $post_id = $_REQUEST['post_id'];
            $post = get_post($post_id);
            $post_type = $post->post_type;
        }

        return self::get_upload_dir($post_type, $folder);
    }

    public static function get_upload_dir($post_type, $folder = null) {

        if (is_null($folder)) {
            $folder = wp_upload_dir();
        }

        $subfolder = NULL;

        switch ($post_type) {

            case 'post' :
                $subfolder = self::$upload_dir_articoli;
                break;

            case 'page' :
                $subfolder = self::$upload_dir_pagine;
                break;

            case Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT :
                $subfolder = self::$upload_dir_contest;
                break;

            case Paranix_Custom_Post_Types_School::PARANIX_SCHOOL_CPT :
                $subfolder = self::$upload_dir_scuole;
                break;

            case PARANIX_SLIDER_CPT :
                $subfolder = self::$upload_dir_slides;
                break;

            default :
                break;
        }

        if (!is_null($subfolder)) {
            $folder['subdir'] = $subfolder;
            $folder['path'] .= $subfolder;
            $folder['url'] .= $subfolder;
        }

        return $folder;
    }

    public static function add_thumbsize() {

        //Size for slider images
        add_image_size('slider-large-cropped', 1920, 760, true);

        //Size for contest images
        add_image_size('contest-home-cropped', 600, 600, true);
        add_image_size('contest-list-cropped', 300, 300, true);
        add_image_size('contest-lightbox-cropped', 900, 900, true);

        //Size for the other post type
        add_image_size('default-small-cropped', 300, 300, true);
        add_image_size('default-medium-cropped', 700, 700, true);
        add_image_size('default-large-cropped', 1000, 1000, true);
        
        add_image_size('facebook', 600, 315);

        return self;
    }
    
    public static function set_facebook_thumb($thumb_size){
        return 'facebook';
    }

    public static function remove_default_thumbsize($sizes) {
        unset($sizes['thumbnail']);
        unset($sizes['medium']);
        unset($sizes['large']);
        return $sizes;
    }

    public static function get_attachment_id_by_url($attachment_url) {

        global $wpdb;
        $attachment_id = false;
        if ('' == $attachment_url) {
            return;
        }

        $upload_dir_paths = wp_upload_dir();
        if (false !== strpos($attachment_url, $upload_dir_paths['baseurl'])) {
            $attachment_url = preg_replace('/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url);
            $attachment_url = str_replace($upload_dir_paths['baseurl'] . '/', '', $attachment_url);
            $attachment_id = $wpdb->get_var($wpdb->prepare("SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url));
        }

        return $attachment_id;
    }

    public function apply_watermark($meta, $id) {

        $parent_post_id = $_REQUEST['post_id'];
        
        $parent_post = get_post($parent_post_id);
        
        
        if ($parent_post->post_type == PARANIX_SLIDER_CPT) {
            
            if (!isset($meta['sizes'])) {
                return $meta;
            }

            $upload_path = wp_upload_dir();
            $path = $upload_path['basedir'];

            //handle the different media upload directory structures
            if (isset($path)) {
                $file = $upload_path['basedir'] .Paranix_Core_MediaManager::$upload_dir_slides . '/'.
                        $meta['sizes']['slider-large-cropped']['file'];
                $water_path =  $upload_path['basedir'] .Paranix_Core_MediaManager::$upload_dir_slides . '/watermark.png';
            } else {
                $file = $upload_path['path'] .Paranix_Core_MediaManager::$upload_dir_slides . '/'.
                        $meta['sizes']['slider-large-cropped']['file'];
                $water_path =  $upload_path['path'] .Paranix_Core_MediaManager::$upload_dir_slides . '/watermark.png';
            }
            
            //list original image dimensions
            list($orig_w, $orig_h, $orig_type) = @getimagesize($file);

            //load watermark - list its dimensions
            $watermark = imagecreatefrompng($water_path);
            list($wm_width, $wm_height, $wm_type) = @getimagesize($water_path);

            //load fullsize image
            $image = wp_load_image($file);
            //if your watermark is a transparent png uncomment below
            imagealphablending($image, true);

            //create merged copy
            imagecopy($image, $watermark, 0, $orig_h - ($wm_height), 0, 0, $wm_width, $wm_height);


            //save image backout
            switch ($orig_type) {
                case IMAGETYPE_GIF:
                    imagegif($image, $file);
                    break;
                case IMAGETYPE_PNG:
                    imagepng($image, $file, 9);
                    break;
                case IMAGETYPE_JPEG:
                    imagejpeg($image, $file, 95);
                    break;
            }

            imagedestroy($watermark);
            imagedestroy($image);

            //return metadata info
            wp_update_attachment_metadata($id, $meta);
            return $meta;
        }
        
        return $meta;
    }

}

//add filter to upload_dir function where an attachment is uploades
add_filter('upload_dir', array('Paranix_Core_MediaManager', 'upload_manager'));

//add the thumb size when the theme is setted up
add_action('after_setup_theme', array('Paranix_Core_MediaManager', 'add_thumbsize'));

//remove default wordpress thumb size
add_filter('intermediate_image_sizes_advanced', array('Paranix_Core_MediaManager', 'remove_default_thumbsize'));

add_filter('wp_generate_attachment_metadata', array('Paranix_Core_MediaManager', 'apply_watermark'), 10, 2);

add_filter('wpseo_opengraph_image_size', array('Paranix_Core_MediaManager', 'set_facebook_thumb'));
