<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_Core_PhotoContest {

    const LIKES_VAR = 'paranix_photo_contest_likes';
    const CHILDNAME_VAR = 'paranix_childname';
    const SCHOOLNAME_VAR = 'paranix_user_childschool';
    const CLASSROOM_VAR = 'paranix_user_school_room';

    /**
     * This function upload in proper way the image to the photo contest custom post type
     * @param File $file
     * @param string $description
     * @param int $user_id
     * @return mixed (boolean||int)
     */
    public function upload_image($file, $description, $user_id, $child_name = null, $school_id = null, $classroom = null) {

        //This var is the trick. Fake the media manager to get my upload dir.
        $GLOBALS['custom_post_type'] = Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT;

        if (!$this->check_data($description)) {

            $upload_overrides = array('test_form' => false);

            $this->require_handle_upload();

            $post_id = $this->insert_post($description, $user_id);

            $this->set_child_name($user_id, $post_id, $child_name);

            $this->update_likes($post_id, 0);
            
            self::set_school_and_classroom($post_id, $user_id, $school_id, $classroom);

            $movefile = wp_handle_upload($file, $upload_overrides);

            if ($movefile['error']) {

                wp_delete_post($post_id, TRUE);

                return false;
            } else {

                $filetype = $movefile['type'];
                $filename = $movefile['file'];
                $attachment_id = $this->insert_attachment($filename, $filetype, $description, $post_id);

                if ($attachment_id != 0) {
                    $this->generate_thumbs($attachment_id, $filename, $post_id);
                } else {
                    wp_delete_post($post_id, TRUE);
                }

                return $attachment_id;
            }
        } else {
            return false;
        }
    }

    /**
     * Update the photo likes
     * @param int $post_id
     * @param int $num_likes
     * @return \Paranix_Core_PhotoContest
     */
    public static function update_likes($post_id, $num_likes) {
        update_post_meta($post_id, self::LIKES_VAR, $num_likes);
        return $this;
    }

    /**
     * Get Most liked photo
     * @return array
     */
    public static function get_most_liked() {
        $photo_contest_obj = new Paranix_Core_PhotoContest();
        return $photo_contest_obj->contest_list(1, 0, Paranix_Core_PhotoContest::LIKES_VAR, 'DESC', 'contest-home-cropped');
    }

    /**
     * Get the contest list photo
     * @param int $ppp
     * @param int $offset
     * @param string $order_by
     * @param string $order
     * @return array
     */
    public function contest_list($ppp = 5, $offset = 0, $order_by = 'date', $order = 'DESC', $thumbnail_size = 'contest-list-cropped') {

        $return = array();
        $contest_list = $this->get_active($ppp, $offset, $order_by, $order);

        foreach ($contest_list->posts as $contest_photo) {

            $featured_image = wp_get_attachment_image_src(
                    get_post_thumbnail_id($contest_photo->ID), $thumbnail_size
            );

            $full_size_image = wp_get_attachment_image_src(
                    get_post_thumbnail_id($contest_photo->ID), 'contest-lightbox-cropped'
            );

            $author_id = $contest_photo->post_author;

            $return[] = array(
                'photo_title' => $contest_photo->post_title,
                'photo_date' => get_the_date('d/m/Y \a\l\l\e g:ia', $contest_photo->ID),
                'photo_description' => $contest_photo->post_excerpt,
                'photo_useravatar' => Paranix_Core_LayoutManager::get_avatar_url($author_id),
                'photo_username' => get_the_author_meta('first_name', $author_id) . ' ' . get_the_author_meta('last_name', $author_id),
                'photo_image_url' => $featured_image[0],
                'photo_image_url_full_size' => $full_size_image[0],
                'photo_url' => get_the_permalink($contest_photo->ID),
                'photo_childname' => get_post_meta($contest_photo->ID, self::CHILDNAME_VAR, TRUE),
                'photo_likes' => get_post_meta($contest_photo->ID, self::LIKES_VAR, TRUE)
            );
        }

        return array_reverse($return);
    }

    public static function upload_via_ajax() {
        $user_id = get_current_user_id();
        $photo_contest_obj = new Paranix_Core_PhotoContest();
        $photo_contest_obj->upload_image(
                $_FILES['paranix_userimage'], 
                'Fantastico! Partecipo al contest del Principe Paranix!', 
                $user_id
                );
    }

    public static function update_via_ajax() {
        if (Paranix_Core_LoaderManager::is_post()) {
            
            $user_id = get_current_user_id();

            $photo_id = $_POST['photo_id'];
            $childname = $_POST['childname'];
            $photo_description = $_POST['photo_description'];
            $school_id = $_POST['school_id'];
            $classroom = $_POST['classroom'];
            
            self::set_school_and_classroom($photo_id, $user_id, $school_id, $classroom);

            $post_id = wp_update_post(array(
                'ID' => $photo_id,
                'post_excerpt' => $photo_description
            ));

            if (is_int($post_id)) {
                update_post_meta($post_id, self::CHILDNAME_VAR, $childname);
            }
        }
    }

    public static function delete_via_ajax() {
        if (Paranix_Core_LoaderManager::is_post()) {
            wp_delete_post($_POST['photo_id']);
        }
    }

    /**
     * 
     */
    public static function update_likes_via_ajax() {

        $url = $_GET['photo_url'];
        $event = $_GET['event'];

        $post_id = url_to_postid($url);

        $current_likes = get_post_meta($post_id, self::LIKES_VAR, TRUE);

        if ($event == 'like') {
            $update_likes = ++$current_likes;
        } else {
            $update_likes = --$current_likes;
        }

        update_post_meta($post_id, self::LIKES_VAR, $update_likes);
    }

    /**
     * Return all contest photo of a user
     * @param id $user_id
     * @return array $user_photo
     */
    public static function get_user_photo($user_id) {

        $photo_contest_args = array(
            'author' => $user_id,
            'orderby' => 'post_date',
            'posts_per_page' => 100,
            'order' => 'ASC',
            'post_status' => array('publish', 'pending'),
            'post_type' => Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT
        );

        $photo_contest_list = new WP_Query($photo_contest_args);
        $return = array();
        foreach ($photo_contest_list->get_posts() as $photo_contest) {
            $temp = array();
            $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($photo_contest->ID), 'contest-list-cropped');

            $temp['photo_id'] = $photo_contest->ID;
            $temp['title'] = $photo_contest->post_title;
            $temp['description'] = $photo_contest->post_excerpt;
            $temp['image'] = $featured_image[0];
            $temp['insert_date'] = date('j-n-Y H:i:s', strtotime($photo_contest->post_date));
            $temp['childname'] = get_post_meta($photo_contest->ID, self::CHILDNAME_VAR, true);
            $temp['likes'] = get_post_meta($photo_contest->ID, self::LIKES_VAR, true);
            $temp['status'] = ($photo_contest->post_status == 'publish') ? 'active' : 'pending';

            $return[] = $temp;
        }

        return $return;
    }

    public function get_child_name($user_id) {
        return get_user_meta($user_id, self::CHILDNAME_VAR, TRUE);
    }

    public static function photocontest_og_title($title) {
        global $post;
        if ($post->post_type === Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT) {
            $title = get_post_meta($post->ID, self::CHILDNAME_VAR, TRUE);
        }
        return $title;
    }

    public function get_photo_of_the_week($ppp = 3) {
        $args = array(
            'posts_per_page' => $ppp,
            'offset' => 0,
            'post_type' => Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT,
            'post_status' => 'publish',
            'meta_key' => self::LIKES_VAR,
            'orderby' => 'meta_value_num date',
            'order' => 'DESC'
        );

        $return = array();

        foreach (query_posts($args) as $photo_contest) {

            $featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($photo_contest->ID), 'contest-list-cropped');

            $temp['title'] = $photo_contest->post_title;
            $temp['image'] = $featured_image[0];
            $temp['photo_url'] = get_the_permalink($photo_contest->ID);

            $return[] = $temp;
        }

        return $return;
    }

    public static function get_photo_school($photo_id) {

        $photo_school = get_post_meta($photo_id, self::SCHOOLNAME_VAR, TRUE);
        $classroom = get_post_meta($photo_id, self::CLASSROOM_VAR, TRUE);

        if (is_numeric($photo_school)) {
            $school = get_post($photo_school);
            return array(
                'school' => $school->post_title,
                'classroom' => $classroom,
            );
        } else {
            return array();
        }
    }

    /**
     * Check if the description is valid
     * @param string $description
     * @return boolean
     */
    private function check_data($description) {
        $error = 0;

        /*if ($description == '' || strlen($description) < 1) {

            ++$error;
            Paranix_Core_FlashMessage::set_message(
                    'Descrizione della foto non sufficiente', Paranix_Core_FlashMessage::ERROR
            );
        }*/

        return ($error == 0) ? false : true;
    }

    /**
     * Generate the thums for attachment insert programmatically
     * @param int $attachment_id
     * @param string $filename
     * @param int $post_id
     */
    private function generate_thumbs($attachment_id, $filename, $post_id) {

        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        $paranix_attachment_data = wp_generate_attachment_metadata($attachment_id, $filename);
        Paranix_Core_LogManager::debug_it($paranix_attachment_data, 1);
        wp_update_attachment_metadata($attachment_id, $paranix_attachment_data);
        update_post_meta($post_id, '_thumbnail_id', $attachment_id, true);
        return $this;
    }

    /**
     * Create the photo contest post
     * @param string $post_title
     * @param int $user_id
     * @return int $post_id
     */
    private function insert_post($description, $user_id) {

        srand(time());
        $post_title = $this->get_child_name($user_id);
        $post_title .= '-' . rand(0, 100);

        $post_args = array(
            'post_title' => $post_title,
            'post_excerpt' => $description,
            'post_type' => Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT,
            'post_status' => 'pending',
            'post_author' => $user_id
        );

        return wp_insert_post($post_args);
    }

    /**
     * Create attachment for the photo contest
     * @param string $filename
     * @param string $wp_file_type
     * @param int $post_id
     * @return int $attachment_id
     */
    private function insert_attachment($filename, $filetype, $attachment_title, $post_id) {
        $wp_upload_dir = wp_upload_dir();
        $attachment = array(
            'guid' => $wp_upload_dir['url'] . '/' . basename($filename),
            'post_mime_type' => $filetype,
            'post_title' => preg_replace('/\.[^.]+$/', '', substr($attachment_title, 0, 30)),
            'post_status' => 'publish'
        );
        return wp_insert_attachment($attachment, $filename, $post_id);
    }

    /**
     * Get active contest photo
     * @param int $ppp
     * @param int $offset
     * @param string $order_by
     * @param string $order
     * @return \WP_Query
     */
    private function get_active($ppp = 5, $offset = 0, $order_by = 'date', $order = 'DESC') {

        $args = array(
            'posts_per_page' => $ppp,
            'offset' => $offset,
            'post_type' => Paranix_Custom_Post_Types_Contest::PARANIX_PHOTO_CONTEST_CPT,
            'post_status' => 'publish',
            'order' => $order
        );

        if ($order_by == 'date') {
            $args['orderby'] = $order_by;
        } else {
            $args['meta_key'] = $order_by;
            $args['orderby'] = 'meta_value_num';
        }

        return new WP_Query($args);
    }

    /**
     * Set the child name of photo
     * @param int $user_id
     * @param int $post_id
     * @param type $child_name
     * @return \Paranix_Core_PhotoContest
     */
    private function set_child_name($user_id, $post_id, $child_name) {
        if (is_null($child_name) || $child_name == '') {
            $user_meta_child_name = get_user_meta($user_id, self::CHILDNAME_VAR, TRUE);
        } else {
            $user_meta_child_name = $child_name;
        }

        update_post_meta($post_id, self::CHILDNAME_VAR, $user_meta_child_name);
        return $this;
    }

    public static function set_school_and_classroom($post_id, $user_id, $school, $classroom) {

        global $wpdb;

        if (!is_numeric($school) || is_null($school)) {
            $user = get_user_by('ID', $user_id);
            $user_role = $user->roles;
            if ($user_role[0] == 'teacher') {

                $select = $wpdb->get_results(sprintf('SELECT post_id, classroom FROM prnx14_teachers_school WHERE user_id %d LIMIT 1', $user->ID));

                update_post_meta($post_id, self::SCHOOLNAME_VAR, $select[0]->post_id);
                update_post_meta($post_id, self::CLASSROOM_VAR, $select[0]->classroom);
            }
            else {
                $user_school = get_user_meta($user_id, self::SCHOOLNAME_VAR, TRUE);
                if (is_numeric($user_school)) {
                    $user_classroom = get_user_meta($user_id, self::CLASSROOM_VAR, TRUE);
                    update_post_meta($post_id, self::SCHOOLNAME_VAR, $user_school);
                    update_post_meta($post_id, self::CLASSROOM_VAR, $user_classroom);
                }
            }
        }
        else{
            update_post_meta($post_id, self::SCHOOLNAME_VAR, $school);
            update_post_meta($post_id, self::CLASSROOM_VAR, $classroom);
        }
    }

    /**
     * This function include wp_handle_upload if not is included
     * @return \Paranix_Core_PhotoContest
     */
    private static function require_handle_upload() {
        if (!function_exists('wp_handle_upload')) {
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
        }
        return $this;
    }

}

add_action('wp_ajax_nopriv_paranix_upload_photocontest', array('Paranix_Core_PhotoContest', 'upload_via_ajax'));
add_action('wp_ajax_paranix_upload_photocontest', array('Paranix_Core_PhotoContest', 'upload_via_ajax'));

add_action('wp_ajax_nopriv_paranix_update_photo', array('Paranix_Core_PhotoContest', 'update_via_ajax'));
add_action('wp_ajax_paranix_update_photo', array('Paranix_Core_PhotoContest', 'update_via_ajax'));

add_action('wp_ajax_nopriv_paranix_delete_photo', array('Paranix_Core_PhotoContest', 'delete_via_ajax'));
add_action('wp_ajax_paranix_delete_photo', array('Paranix_Core_PhotoContest', 'delete_via_ajax'));

add_action('wp_ajax_nopriv_paranix_update_like', array('Paranix_Core_PhotoContest', 'update_likes_via_ajax'));
add_action('wp_ajax_paranix_update_like', array('Paranix_Core_PhotoContest', 'update_likes_via_ajax'));

add_filter('wpseo_opengraph_title', array('Paranix_Core_PhotoContest', 'photocontest_og_title'));
