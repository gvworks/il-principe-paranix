<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_Core_ShortcodeManager {
    
    /**
     * Set the childname metabox
     */
    public static function set_shortcode_product_generator(){
        add_meta_box(
                'wpt_paranix_product_generator', 
                'Genera shortcode prodotti', 
                array('Paranix_Core_ShortcodeManager', 'wpt_paranix_product_generator'), 
                'page',
                'normal', 
                'default'
       );
    }
    
    /**
     * 
     * @global type $post
     */
    public static function wpt_paranix_product_generator() {
        wp_enqueue_script('prodotti', PARANIX_ASSETS_FOLDER.'/custom/js/wp-admin/product/product.js', array(), false, true);
        $option = '<option></option>';
        foreach (Paranix_Core_ProductManager::get_product_select() as $option_select){
            $option .= sprintf('<option value="%s">%s</option>', 
                    $option_select['paranix_product_id'], 
                    $option_select['paranix_product_title']
            );
        }
        $select = sprintf('<select id="product_shortcode_generator" name="product_shortcode_generator">%s</select>', $option);
        $color_select = '<label for="product_shortcode_generator_color">Seleziona il colore del titolo del box prodotto</label>';
        $color_select .= '<select id="product_shortcode_generator_color">';
        $color_select .= '<option value="green">Verde</option>';
        $color_select .= '<option value="purple">Viola</option>';
        $color_select .= '</select>';
        
        echo $select.'<br/>';
        echo $color_select;
        
        echo '<br/><br/>Copia e incolla all\'interno dell\'articolo il codice generato.<hr/><span class="updated" id="paranix_shortcode_generate"></span>';
    }
    
    public static function product_shortcode($atts){
        extract( shortcode_atts(
		array(
		), $atts )
	);
        
        $product_post = get_post($atts['id']);
        $color_title = (isset($atts['color'])) ? $atts['color'] : 'green';
        ob_start();
        include dirname(__FILE__)."/../layouts/partials/products/products-short.php";
        $product_shortcode = ob_get_contents();
        ob_end_clean();
        return $product_shortcode;
    }
    

}

add_action('do_meta_boxes', array('Paranix_Core_ShortcodeManager', 'set_shortcode_product_generator'));
add_shortcode( 'paranix-product', array('Paranix_Core_ShortcodeManager', 'product_shortcode') );