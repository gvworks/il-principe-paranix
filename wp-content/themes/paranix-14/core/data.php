<?php

add_action('wp_ajax_nopriv_paranix_data_load_comuni', 'paranix_data_load_comuni');
add_action('wp_ajax_paranix_data_load_comuni', 'paranix_data_load_comuni');

function paranix_data_load_comuni(){
    $term = $_GET['term'];
    global $wpdb;
    $comuni_select = 'SELECT c.id, c.name, p.sigla FROM prnx14_mne_comuni as c '
            . 'INNER JOIN prnx14_mne_province as p ON p.id = c.province_id '
            . 'WHERE LOWER(c.name) LIKE "'.strtolower($term).'%" LIMIT 7';
    
    $return = array();
    foreach ($wpdb->get_results($comuni_select) as $result){
        $return[] = array("label" => $result->name.' ('.$result->sigla.')', 'id' => $result->id);
    }
    print json_encode($return);
    die();
}


add_action('wp_ajax_nopriv_paranix_data_load_scuole', 'paranix_data_load_scuole');
add_action('wp_ajax_paranix_data_load_scuole', 'paranix_data_load_scuole');

add_action('wp_ajax_nopriv_paranix_data_load_scuole_classi', 'paranix_data_load_scuole_classi');
add_action('wp_ajax_paranix_data_load_scuole_classi', 'paranix_data_load_scuole_classi');

function paranix_data_load_scuole(){
    $comune_id = $_GET['term'];
    $args = array(
        'post_type'   => Paranix_Custom_Post_Types_School::PARANIX_SCHOOL_CPT,
        'meta_key' => 'paranix_school_comune_id',
        'meta_value' => $comune_id
    );
    $schools = get_posts($args);
    $return = array();
    foreach ($schools as $school){
        $return[] = array(
            array("label" => $school->post_name, 'id' => $school->ID)
        );
    }
    print json_encode($return);
    die();
}

function paranix_data_load_scuole_classi(){
    $post_id = $_GET['term'];
    $post_meta = get_post_meta($post_id, 'paranix_school_classroom_teachers', TRUE);
    $return = array();
    foreach ($post_meta as $classroom){
        $return[] = array(
            array("label" => $classroom['classroom'], 'id' => $classroom['classroom'])
        );
    }
    print json_encode($return);
    die();
}

function paranix_data_get_comune($id){
    global $wpdb;
    $comune_select =  'SELECT c.id, c.name, p.sigla FROM prnx14_mne_comuni as c '
            . 'INNER JOIN prnx14_mne_province as p ON p.id = c.province_id '
            . 'WHERE c.id = '.$id;
    
    return $wpdb->get_results($comune_select);
}