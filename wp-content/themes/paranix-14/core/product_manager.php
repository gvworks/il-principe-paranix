<?php

class Paranix_Core_ProductManager{
    
    public static function get_product_select(){
        $args = array(
            'post_type' => Paranix_Custom_Post_Types_Product::PARANIX_PRODUCT_CPT
        );
        $returnable_products = array();
        $total_products = new WP_Query($args);
        while ($total_products->have_posts()) {
            $total_products->the_post();
            $returnable_products[] = array(
                'paranix_product_id' => get_the_ID(),
                'paranix_product_title' => get_the_title()
            );
        }
        return $returnable_products;
    }
    
}

