<?php

class Paranix_Core_LogManager{
    
    public static function debug_it($message, $env){
        if(WP_ENV >= $env){
            error_log($message, 1, 'mnemosiagency@gmail.com');
        }
        return false;
    }
}
