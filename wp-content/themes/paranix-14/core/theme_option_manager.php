<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_Core_ThemeOptionManager{
    public static function load(){
        include_once get_template_directory().'/option-tree/ot-loader.php';
    }
}

add_filter( 'ot_theme_mode', '__return_true' );
add_filter( 'ot_meta_boxes', '__return_true' );
add_filter( 'ot_show_new_layout', '__return_false' );
