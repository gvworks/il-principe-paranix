<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_Core_LoaderManager {

    private static $core_folder = '/core/';
    private static $custom_post_types_folder = '/custom_post_types/';
    private static $page_manager_folder = '/page_manager/';
    private static $admin_pages_folder = '/admin_pages/';
    
    public function __construct() {
        $this->load_core()
                ->load_custom_post_types()
                ->load_admin_pages();
    }

    public function load_core() {
        foreach ($this->set_core() as $core_file){
            if(file_exists($this->get_core_path() . $core_file)){
                require_once $this->get_core_path() . $core_file;
            }
        }
        return $this;
    }

    public static function get_core_path() {
        return get_template_directory() . self::$core_folder;
    }

    public function load_custom_post_types() {
        foreach ($this->set_custom_post_types() as $custom_post_type_file){
            if(file_exists($this->get_custom_post_types_path() . $custom_post_type_file)){
                require_once $this->get_custom_post_types_path() . $custom_post_type_file;
            }
        }
        return $this;
    }

    public static function get_custom_post_types_path() {
        return get_template_directory() . self::$custom_post_types_folder;
    }
    
    public function load_admin_pages() {
        foreach ($this->set_admin_pages() as $admin_pages_file){
            if(file_exists($this->get_admin_pages_path() . $admin_pages_file)){
                require_once $this->get_admin_pages_path() . $admin_pages_file;
            }
        }
        return $this;
    }
    
    public static function get_admin_pages_path() {
        return get_template_directory() . self::$admin_pages_folder;
    }
    
    public static function include_page_manager($template){
        require_once self::get_page_manager_path() . 'global.php';
        $filename = basename($GLOBALS['template']);
        if(file_exists(self::get_page_manager_path() . $filename)){
            require_once self::get_page_manager_path() . $filename;
        }
        return $template;
    }
    
    public static function get_page_manager_path() {
        return get_template_directory() . self::$page_manager_folder;
    }
    
    /**
     * Check if request is a post
     * @return boolean
     */
    public static function is_post(){
        return ($_SERVER['REQUEST_METHOD'] === 'POST');
    }
    
    /**
     * Redirect to page by title
     * @param string $title
     */
    public static function redirect_to_page_by_title($title){
        wp_redirect(self::get_page_url_by_title($title));
    }
    
    /**
     * Return the page url by title
     * @param string $title
     * @return string $uel
     */
    public static function get_page_url_by_title($title, $query_string = null){
        $return = '';
        if(is_array($query_string)){
            array_walk_recursive($query_string, function($item, $key){
                    $_SESSION['paranix_page_url_temp'] .= $key.'='.$item.'&';

            });
            $return = '?'.rtrim($_SESSION['paranix_page_url_temp'], '&');
            unset($_SESSION['paranix_page_url_temp']);
        }
        return esc_url( get_permalink( get_page_by_title($title))).$return;
    }

    /**
     * Return all core resources active.
     * @return array $core_resource
     */
    private function set_core() {
        return array(
            'data.php',
            'email_manager.php',
            'flash_message.php',
            'layout_manager.php',
            'log_manager.php',
            'media_manager.php',
            'photo_contest.php',
            'product_manager.php',
            'slider_manager.php',
            'shortcode_manager.php',
            'theme_option_manager.php',
            'user_manager.php'
        );
    }

    /**
     * Return all custom post type resource active.
     * @return array $custom_post_type_resource
     */
    private function set_custom_post_types() {
        return array(
            'photo_contest.php',
            'product.php',
            'school.php',
            'slider.php'
        );
    }
    
    private function set_admin_pages(){
        return array(
            'teachers.php'
        );
    }

}

add_action( 'template_include', array('Paranix_Core_LoaderManager', 'include_page_manager'), 1000 );