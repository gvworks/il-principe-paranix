<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_Core_SliderManager {
    
    /**
     * Get the slides active
     * @param int $num_slide
     * @return array
     */
    public static function get($num_slide = 10){
        $args = array(
            'post_type' => PARANIX_SLIDER_CPT, 
            'posts_per_page' => $num_slide
        );
        $returnable_slides = array();
        $total_slides = new WP_Query($args);
        while ($total_slides->have_posts()) {
            $total_slides->the_post();
            $slide_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'slider-large-cropped');
            $slide_meta_url = get_post_meta(get_the_ID(), 'paranix_slider_url');
            $slide_meta_order = get_post_meta(get_the_ID(), 'paranix_slider_order');
            $returnable_slides[] = array(
                'paranix_slide_title' => get_the_title(),
                'paranix_slide_image' => $slide_image[0],
                'paranix_slide_url' => $slide_meta_url[0],
                'paranix_slide_order' => $slide_meta_order[0]
            );
        }

        usort($returnable_slides, function ($a, $b) {
            if ($a['paranix_slide_order'] == $b['paranix_slide_order']) return 0;
            return ($a['paranix_slide_order'] < $b['paranix_slide_order']) ? -1 : 1;
        });

        return $returnable_slides;
    }
}
