<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
class Paranix_Core_UserManager {
    
    private static $session_temporary_name = 'PARANIX_USER_NAME_TEMP';
    private static $session_came_from_registration = 'PARANIX_USER_CAME_FROM_REGISTRATION';

    /**
     * Register the user
     * @param array $submitted_form_post
     * @return mixed (boolean||int)
     */
    public function process_registration($submitted_form_post) {

        if (empty($submitted_form_post)) {
            Paranix_Core_FlashMessage::set_message('Errore nel processare la richiesta.');
        }

        if (!$this->check_registration_data($submitted_form_post)) {

            if (!email_exists($submitted_form_post['paranix_useremail'])) {
                $username = $this->generate_username_from_email($submitted_form_post['paranix_useremail']);
                $userdata = array(
                    'user_login' => $username,
                    'user_pass' => $submitted_form_post['paranix_userpassword'],
                    'user_email' => $submitted_form_post['paranix_useremail'],
                    'first_name' => $submitted_form_post['paranix_userfirstname'],
                    'last_name' => $submitted_form_post['paranix_userlastname'],
                    'role'    => ($submitted_form_post['paranix_teacher'] == 1) ? 'teacher' : 'None'
                );
                
                $_SESSION[self::$session_temporary_name] = $submitted_form_post['paranix_userfirstname'];
                $_SESSION[self::$session_came_from_registration] = 1;

                $user_id = wp_insert_user($userdata);

                if (is_int($user_id)) {
                    $this->register_metadata($user_id, $submitted_form_post);
                    if($submitted_form_post['paranix_teacher'] == 1){
                        $this->join_teacher_to_school($user_id, $submitted_form_post['paranix_user_school'], $submitted_form_post['paranix_user_school_room']);
                    }
                    return $user_id;
                } else {
                    Paranix_Core_FlashMessage::set_message('Il tuo indirizzo email risulta già essere registrato.');
                    return false;
                }
            }
            else{
                Paranix_Core_FlashMessage::set_message('Il tuo indirizzo email risulta già essere registrato.');
            }

            return false;
        }
    }

    /**
     * Authenticate user by email||username and password
     * @param string $username
     * @param string $password
     * @return mixed (boolean||Wp_User $Obj)
     */
    public function login($username, $password) {
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $temp_user = get_user_by_email($username);
            $creds['user_login'] = $temp_user->user_login;
        } else {
            $creds['user_login'] = $username;
        }

        $check_user = get_user_by('login', $creds['user_login']);

        if ($check_user instanceof WP_User) {
            $user_status = get_user_meta($check_user->ID, 'paranix_user_status', TRUE);
            $user_activation_token = get_user_meta($check_user->ID, 'paranix_user_activation_key', TRUE);

            if ($user_status != 1 && $user_activation_token != '') {
                return false;
            }
        }

        $creds['user_password'] = $password;
        $creds['remember'] = true;
        $user = wp_signon($creds, false);
        if (is_wp_error($user)) {
            return false;
        }
        return $user;
    }

    /**
     * Confirm a user registration
     * @param string $access_token
     * @return boolean
     */
    public function confirm_registration($access_token) {

        $query_user_meta = array(
            'meta_query' => array(
                'relation' => 'AND',
                0 => array(
                    'key' => 'paranix_user_activation_key',
                    'value' => $access_token
                ),
                1 => array(
                    'key' => 'paranix_user_status',
                    'value' => 0
                )
            )
        );

        $user_query = new WP_User_Query($query_user_meta);

        $users = $user_query->get_results();

        if (is_int($users[0]->ID)) {
            update_user_meta($users[0]->ID, 'paranix_user_activation_key', '');
            update_user_meta($users[0]->ID, 'paranix_user_status', 1);
            return true;
        }
        return false;
    }
    
    public function check_user($userID){
        if(filter_var($userID, FILTER_VALIDATE_EMAIL)){
            $user = get_user_by('email', $userID);
        }
        else{
            $user = get_user_by('login', $userID);
        }
        
        if($user instanceof WP_User){
            return $user;
        }
        else{
            Paranix_Core_FlashMessage::set_message("L'indirizzo email inserito non è valido'");
            return false;
        }
    }
    
    private function join_teacher_to_school($user_id, $school_id, $classroom){
        global $wpdb;
        $wpdb->insert('prnx14_teachers_school', array('user_id' => $user_id, 'post_id' => $school_id, 'classroom' => $classroom));
    }
    
    /**
     * Return the activation url for user
     * @param int $user_id
     * @return string $activation_link
     */
    public static function get_activation_link($user_id){
        $paranix_user_meta  = get_user_meta($user_id);
        return Paranix_Core_LoaderManager::get_page_url_by_title('Utenti - Funzioni speciali', 
                array('activation_key' => $paranix_user_meta['paranix_user_activation_key'][0], 'action' => 'useractivation')
                );
    }
    
    public static function get_lost_password_link($user_email){
        $user = get_user_by('email', $user_email);
        return Paranix_Core_LoaderManager::get_page_url_by_title('Recupera Password', 
                array('key' => Paranix_Core_UserManager::url_codified_user_email($user->user_email, $user->ID))
                );
    }
    
    /**
     * Codify and convert email
     * @param string $email
     * @param int $user_id
     * @return string $url_codified_user_email
     */
    public static function url_codified_user_email($email, $user_id){
        $params = array('email' => $email, 'uid' => $user_id);
        return 
            rawurlencode(
                base64_encode(
                        http_build_query($params)
                        )
                );
    }
    
    /**
     * Decodify and convert email
     * @param string $email
     * @return string $decodified_user_email
     */
    public static function url_decodified_user_email($email){
        $params = base64_decode(rawurldecode($email));
        parse_str($params, $output);
        return $output;
    }

    /**
     * Check if user came from registration page
     * @param boolean $delete_after
     * @return boolean
     */
    public static function came_from_registration($delete_after = FALSE){
        $session_came_from_registration = $_SESSION[self::$session_came_from_registration];
        //if is set to true the global session var is destroyed
        if($delete_after == TRUE){
            unset($_SESSION[self::$session_came_from_registration]);
        }
        return ($session_came_from_registration == 1) ? TRUE : FALSE;
    }

    /**
     * Get the temporary stored user firstname
     * @param boolean $delete_after
     * @return boolean
     */
    public static function get_temporary_stored_name($delete_after = FALSE){
        $session_temporary_name = $_SESSION[self::$session_temporary_name];
        //if is set to true the global session var is destroyed
        if($delete_after == TRUE){
            unset($_SESSION[self::$session_temporary_name]);
        }
        return $session_temporary_name;
    }

    /**
     * Generate username from email address
     * @example mail[at]example.com => mailexamplecom
     * @param string $email
     * @return string
     */
    private function generate_username_from_email($email) {
        $email_parts = explode("@", $email);
        $email_username = $email_parts[0];
        $email_host = explode(".", $email_parts[1]);
        return $email_username . implode('', $email_host);
    }

    /**
     * Set the user metadata
     * @param int $user_id
     * @param array $userdata
     */
    private function register_metadata($user_id, $userdata) {

        $activation_token = $this->generate_access_token($userdata['paranix_useremail']);

        update_user_meta($user_id, 'paranix_user_tel', $userdata['paranix_usertel']);
        update_user_meta($user_id, 'paranix_user_comune_id', $userdata['paranix_comune_id']);
        update_user_meta($user_id, 'paranix_childname', $userdata['paranix_childname']);
        update_user_meta($user_id, 'paranix_user_activation_key', $activation_token);
        update_user_meta($user_id, 'paranix_user_status', 0);
        //Se l'utente fa parte del progetto didattico gli setto la scuola
        if ($userdata['paranix_school_registrazione'] == 1) {
            update_user_meta($user_id, 'paranix_user_childschool', $userdata['paranix_user_school']);
            update_user_meta($user_id, 'paranix_user_school_room', $userdata['paranix_user_school_room']);
        }
    }

    /**
     * Generate univoque access token email based
     * @param string $email
     * @return string
     */
    private function generate_access_token($email) {
        return sha1($email . time());
    }

    /**
     * Check if all data of registration are correct.
     * @param array $user
     * @return boolean
     */
    private function check_registration_data($user) {

        $error = 0;

        if ($user['paranix_userfirstname'] == '' || strlen($user['paranix_userfirstname']) < 3) {
            ++$error;
            Paranix_Core_FlashMessage::set_message('Nome non valido');
        }

        if ($user['paranix_userlastname'] == '' || strlen($user['paranix_userlastname']) < 3) {
            ++$error;
            Paranix_Core_FlashMessage::set_message('Cognome non valido');
        }

        if (!filter_var($user['paranix_useremail'], FILTER_VALIDATE_EMAIL)) {
            ++$error;
            Paranix_Core_FlashMessage::set_message('Indirizzo email non valido.');
        }

        if ($user['paranix_userpassword'] == '' || strlen($user['paranix_userpassword']) < 5) {
            ++$error;
            Paranix_Core_FlashMessage::set_message('Password non corretta.');
        }

        if (($user['paranix_childname'] == '' || strlen($user['paranix_childname']) < 4) && $user['paranix_teacher'] != 1) {
            ++$error;
            Paranix_Core_FlashMessage::set_message('Nome del bambino non valido.');
        }

        if (($user['paranix_usertel'] == '' || strlen($user['paranix_usertel']) < 9) && $user['paranix_teacher'] != 1) {
            ++$error;
            Paranix_Core_FlashMessage::set_message('Numero di telefono non valido.');
        }

        if ($user['paranix_comune_id'] == '' || !is_numeric($user['paranix_comune_id'])) {
            ++$error;
            Paranix_Core_FlashMessage::set_message('Comune di residenza non valido.');
        }

        if ($user['paranix_school_registrazione'] == 1) {
            if ($user['paranix_user_school'] == '' || !is_numeric($user['paranix_user_school'])) {
                ++$error;
                Paranix_Core_FlashMessage::set_message('Scuola selezionata non valida.');
            }
            if($user['paranix_user_school_room'] == ''){
                ++$error;
                Paranix_Core_FlashMessage::set_message('Classe selezionata non valida.');
            }
        }

        return ($error == 0) ? false : true;
    }

}
