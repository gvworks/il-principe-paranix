<?php

class Paranix_Core_FlashMessage{
    
    const ERROR = 'danger';
    const SUCCESS = 'success';
    const WARNING = 'warning';
    
    private static $session_name_var = 'PARANIX_MESSAGE_VAR';

    public static function set_message($message, $type = Paranix_Core_FlashMessage::ERROR){
        $_SESSION[self::$session_name_var][] = array($type, $message);
        return $this;
    }
    
    public static function has_message(){
        if(count($_SESSION[self::$session_name_var]) > 0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    
    public static function return_formatted_message(){
        foreach (self::get_messages() as $message){
            $label = ($message[0] == 'success') ? 'Perfetto' : 'Errore';
            $html .= sprintf("
                    <div class='alert alert-%s fade in' role='alert'>
                        <button type='button' class='close' data-dismiss='alert'>
                            <span aria-hidden='true'>×</span><span class='sr-only'>Close</span>
                        </button>
                        <strong>%s!</strong> %s<br/>
                    </div>", $message[0], $label, $message[1]);
        }
        return $html;
    }
    
    private static function get_messages(){
        $temp_message = $_SESSION[self::$session_name_var];
        unset($_SESSION[self::$session_name_var]);
        return $temp_message;
    }
    
}

