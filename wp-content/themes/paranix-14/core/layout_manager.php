<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_Core_LayoutManager{
    
    public static function include_partials($filename, $subfolder = null){
        if(! is_null($subfolder)){
            $file_path = self::get_layout_path() . DIRECTORY_SEPARATOR . $subfolder . DIRECTORY_SEPARATOR . $filename;
        }
        else{
            $file_path = self::get_layout_path() . DIRECTORY_SEPARATOR .  $filename;
        }
        if(file_exists( $file_path )){
            include $file_path;
        }
    }
    
    public static function init_menus() {
        register_nav_menus(
                array(
                    'header-menu' => __('Header Menu'),
                    'product-menu' => __('Menu Prodotti'),
                    'contest-menu' => __('Menu Contest'),
                )
        );
    }
    
    public static function header_menu_configuration() {
        $header_menu_config = array(
            'theme_location' => 'header-menu',
            'menu' => false,
            'menu_class' => 'nav navbar-nav',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        );
        return $header_menu_config;
    }
    
    public static function contest_menu_configuration() {
        $header_menu_config = array(
            'theme_location' => 'contest-menu',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => 'contest-nav',
            'menu' => false,
            'menu_class' => 'nav navbar-nav navbar-right',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        );
        return $header_menu_config;
    }
    
    public static function product_menu_configuration() {
        $header_menu_config = array(
            'theme_location' => 'product-menu',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => 'product-nav',
            'menu' => false,
            'menu_class' => 'nav navbar-nav navbar-right',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        );
        return $header_menu_config;
    }
    
    public static function load_jquery(){
        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui');
        wp_enqueue_script( 'jquery-ui-autocomplete' );
    }
    
    public static function disable_admin_bar(){
        global $current_user;
        if(is_user_logged_in()){
            $user_role = $current_user->roles;
            if($user_role[0] == 'teacher' || $user_role[0] == 'None' || $user_role[0] == 'none' || $user_role[0] == ''){
                show_admin_bar(false);
            }
        }
    }
    
    public static function add_page_excerpt(){
        add_post_type_support( 'page', 'excerpt' );
    }
    
    public static function get_avatar_url($user_id, $size = 32){
        $avatar = get_avatar( $user_id, $size );
        preg_match("/src='(.*?)'/i", $avatar, $matches);
        return $matches[1];
    }

    private static function get_layout_path(){
        return get_template_directory() . DIRECTORY_SEPARATOR . 'layouts/partials/';
    }
    
}

add_action('init', array('Paranix_Core_LayoutManager', 'init_menus'));
add_action( 'init', array('Paranix_Core_LayoutManager', 'add_page_excerpt') );
add_action('init', array('Paranix_Core_LayoutManager', 'disable_admin_bar'));
add_action('wp_head', array('Paranix_Core_LayoutManager', 'load_jquery'));
add_theme_support('post-thumbnails');
