<?php

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

class Paranix_Core_EmailManager{
    
    /**
     * Set the content type for the hook wp_mail_content_type
     * @return string
     */
    public static function get_content_type(){
        return 'text/html';
    }
    
    /**
     * Set the header send name
     * @return array
     */
    private static function get_header_send_name(){
        return array('From: Web Master - Il Principe Paranix <webmaster@ilprincipeparanix.it>');
    }
    
    public static function send_user_activation($user_id){
        $paranix_user_info = get_userdata($user_id);
        $paranix_user_meta  = get_user_meta($user_id);
        //$paranix_user_activation_url = 
            //    'http://www.ilprincipeparanix.it/conferma-iscrizione/?activation_key='.$paranix_user_meta['paranix_user_activation_key'][0];
        $paranix_user_activation_url = Paranix_Core_UserManager::get_activation_link($user_id);
        ob_start();
        include_once dirname(__FILE__)."/../layouts/emails/activation_user.php";
        $activation_user_email_body = ob_get_contents();
        ob_end_clean();
        wp_mail(
                $paranix_user_info->user_email, 
                'Attivazione utente - Il Principe Paranix', 
                $activation_user_email_body, 
                self::get_header_send_name()
        );
    }
    
    public static function send_lost_password($user_id){
        $paranix_user_info = get_userdata($user_id);
        $paranix_user_lost_email_url = Paranix_Core_UserManager::get_lost_password_link($paranix_user_info->user_email);
        ob_start();
        include_once dirname(__FILE__)."/../layouts/emails/lost_password.php";
        $lost_password_user_email_body = ob_get_contents();
        ob_end_clean();
        wp_mail(
                $paranix_user_info->user_email,
                'Recupera password - Il Principe Paranix', 
                $lost_password_user_email_body, 
                self::get_header_send_name()
        );
    }
}

//Set the email content type
add_filter( 'wp_mail_content_type', array('Paranix_Core_EmailManager', 'get_content_type') );