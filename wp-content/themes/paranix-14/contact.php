<?php
/*
  Template Name: Pagina di contatti
 */

Paranix_Core_LayoutManager::include_partials('header.php');
?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12 boxshadow padding-top-bottom">
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <h3 class="bigh3title"><?= get_the_title(); ?></h2>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    <?php
        Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>