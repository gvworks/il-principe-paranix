<?php
/*
  Template Name: Area utente
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */

//Controllo che l'utente che sta visitando questa pagina ne viene dalla pagina di registrazione.
if(! is_user_logged_in()){
    Paranix_Core_LoaderManager::redirect_to_page_by_title('Sign up');
}
global $current_user;
$user_role = $current_user->roles;

Paranix_Core_LayoutManager::include_partials('header.php');
Paranix_Core_LayoutManager::include_partials('nav.php');

?>
<hr/>
    <div class="container-fluid">
        <div class="col-sm-12 col-md-8 col-md-offset-2 castle-before">
            <h3 class="bigh3title">Benvenuto nell'area utente</h3>
            <hr/>
            <?php
                if (Paranix_Core_FlashMessage::has_message()) {
                    print Paranix_Core_FlashMessage::return_formatted_message();
                }
                if($user_role[0] == 'teacher'){
                    Paranix_Core_LayoutManager::include_partials('tabs-teacher.php', 'user-area');
                }
                else{
                    Paranix_Core_LayoutManager::include_partials('tabs-user.php', 'user-area');
                }
            ?>
        </div>
    </div>
    <hr/>
    <?php
    Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>
