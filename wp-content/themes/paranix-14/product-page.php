<?php
/*
  Template Name: Pagina Prodotto
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */
    Paranix_Core_LayoutManager::include_partials('header.php');
?>
<body>
    <?php
        Paranix_Core_LayoutManager::include_partials('nav.php');
        Paranix_Core_LayoutManager::include_partials('carousel.php');
    ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12 boxshadow">
            <?php Paranix_Core_LayoutManager::include_partials('breadcrumb.php', 'products'); ?>
            <div class="container">
                <div class="col-md-12">
                     <?php
                        if (have_posts()) {
                            while (have_posts()) {
                                the_post();
                                Paranix_Core_LayoutManager::include_partials('view.php', 'products');
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <?php
        Paranix_Core_LayoutManager::include_partials('footer.php');
    ?>
</body>
</html>