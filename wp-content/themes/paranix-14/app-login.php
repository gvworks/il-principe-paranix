<?php

/*
  Template Name: App login
 */

/**
 * @author Marco Barrella
 * @copyright (c) 2014, Marco Barrella
 */


if (Paranix_Core_LoaderManager::is_post()) {
    
    $json = array();
    
    $paranix_user_manager_login = new Paranix_Core_UserManager();
    $paranix_auth_user = $paranix_user_manager_login->login($_POST['username'], $_POST['password']);
    
    if( $paranix_auth_user instanceof WP_User){
        if($_FILES['photo']['name']){
            
            $paranix_photo_contest_manager = new Paranix_Core_PhotoContest();
            $paranix_attachment_id = $paranix_photo_contest_manager->upload_image(
                    $_FILES['photo'], 
                    $_POST['paranix_photo_description'], 
                    $paranix_auth_user->ID
             );
            
            
            if(is_int($paranix_attachment_id)){
                $json = array('feedback' => 2, 'message' => 'success');
            }
            else{
                $json = array('feedback' => 1, 'message' => 'upload error');
            }
            
        }
        else{
            $json = array('feedback' => 1, 'message' => 'upload error');
        }
    }
    else {
        $json = array('feedback' => 0, 'message' => 'login error');
    }
    wp_send_json($json);
}