jQuery(document).ready(function($) {
    
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

jQuery(document).ready(function($){
    
    $(".gallery-video").click(function(){
        var new_src = $(this).children('img').data('target');
        var new_title = $(this).children('img').attr('alt');
        $(this).parent().parent().find('.embed-responsive-item').attr('src', new_src);
        $(this).parent().parent().find('.text-success').text(new_title);
        $(".gallery-video").removeClass('active');
        $(this).addClass('active');
        return false;
    });
    
});