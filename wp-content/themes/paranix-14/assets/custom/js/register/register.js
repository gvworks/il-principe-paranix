/* 
 * @author Marco Barrella
 */

jQuery(function($) {
    
    $("#paranix_user_school").change(function() {
        var school_id = $(this).val();
        get_classrooms(school_id);
    });
    
    $("#paranix_userimage").change(function() {
        readURL(this);
    });

    $("#paranix_comune").on('change', function() {
        $('#paranix_school_hide').show('slow').removeClass('hide');
    });

    $.validate({
        modules: 'location, date, security, file'
    });

    $("#paranix_comune").autocomplete({
        source: function(request, response) {
            var ajaxURL = '/wp-admin/admin-ajax.php';
            // here is where the request will happen
            jQuery.ajax({
                type: 'GET',
                url: ajaxURL, //ajax controller request
                data: {
                    'action': 'paranix_data_load_comuni', //action invoked
                    'fn': 'paranix_data_load_comuni', //function required
                    'term': request.term//if of post required
                },
                dataType: "json",
                success: function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.label,
                            id: item.id
                        };
                    }));
                },
            });
        },
        minLength: 3,
        select: function(event, ui) {
            $('#paranix_comune_id').val(ui.item.id);
            get_school(ui.item.id);
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };


    $("#paranix_schoolname").autocomplete({
        source: function(request, response) {
            var ajaxURL = '/wp-admin/admin-ajax.php';
            // here is where the request will happen
            jQuery.ajax({
                type: 'GET',
                url: ajaxURL, //ajax controller request
                data: {
                    'action': 'paranix_data_load_scuole', //action invoked
                    'fn': 'paranix_data_load_scuole', //function required
                    'term': request.term//if of post required
                },
                dataType: "json",
                success: function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.label,
                            id: item.id
                        };
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            $('#paranix_school_registrazione').val(ui.item.id);
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {

                $('#paranix_user_preview_image').attr('src', e.target.result).attr('width', 200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function get_school(comune_id){
        
        var ajaxURL = '/wp-admin/admin-ajax.php';
        // here is where the request will happen
        jQuery.ajax({
            type: 'GET',
            url: ajaxURL, //ajax controller request
            data: {
                'action': 'paranix_data_load_scuole', //action invoked
                'fn': 'paranix_data_load_scuole', //function required
                'term': comune_id//if of post required
            },
            dataType: "json",
            success: function(data) {
                $("#paranix_user_school")
                        .find('option')
                        .remove()
                        .end()
                        .append($('<option>', {
                    value: null,
                    text: 'Scegli scuola'
                }));
                for (var key in data) {
                    var subdata = data[key];
                    for(var subkey in subdata){
                        /// jquerify the DOM object 'o' so we can use the html method
                        $("#paranix_user_school").append($('<option>', {
                            value: subdata[subkey].id,
                            text: subdata[subkey].label
                        }));
                    }
                }
            }
        });
    }
    
    function get_classrooms(school_id){
        console.log('entro');
        
        var ajaxURL = '/wp-admin/admin-ajax.php';
        // here is where the request will happen
        jQuery.ajax({
            type: 'GET',
            url: ajaxURL, //ajax controller request
            data: {
                'action': 'paranix_data_load_scuole_classi', //action invoked
                'fn': 'paranix_data_load_scuole_classi', //function required
                'term': school_id//if of post required
            },
            dataType: "json",
            success: function(data) {
                $("#paranix_user_school_room")
                        .find('option')
                        .remove()
                        .end()
                        .append($('<option>', {
                    value: null,
                    text: 'Scegli classe'
                }));
                for (var key in data) {
                    var subdata = data[key];
                    for(var subkey in subdata){
                        /// jquerify the DOM object 'o' so we can use the html method
                        $("#paranix_user_school_room").append($('<option>', {
                            value: subdata[subkey].id,
                            text: subdata[subkey].label
                        }));
                    }
                }
            }
        });
    }

});