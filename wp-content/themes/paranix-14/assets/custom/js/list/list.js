/* 
 * @author Marco Barrella
 */

jQuery(function($) {
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        //$(this).ekkoLightbox();
    });
    
    //init_masonry();

    function init_masonry() {
        var $container = $('#gallery-container');
        var gutter = 12;
        var min_width = 250;
        $container.imagesLoaded(function() {
            $container.masonry({
                itemSelector: '.contest-item-photo',
                gutterWidth: gutter,
                isAnimated: true,
                columnWidth: function(containerWidth) {
                    var num_of_boxes = (containerWidth / min_width | 0);
                    var box_width = (((containerWidth - (num_of_boxes - 1) * gutter) / num_of_boxes) | 0);
                    if (containerWidth < min_width) {
                        box_width = containerWidth;
                    }
                    $('.contest-item-photo').width(box_width);
                    return box_width;
                }
            });
        });
    }

});