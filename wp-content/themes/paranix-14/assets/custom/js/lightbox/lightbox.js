jQuery(function($) {

    $("#social_lightbox").hide();

    $(".gallery-items").on('click', function(e) {
        $("#social_lightbox").fadeIn('slow');
        set_data(this);
    });

    $(".social_lightbox_direction").on('click', function(e) {
        e.preventDefault();
        var direction = $(this).data('direction');
        get_data(direction);
    });

    $(".modal-meta-top button.close").on('click', function(e) {
        $("#social_lightbox_title").text('');
        $("#social_lightbox_image").attr('src', '');
        $("#description").text('');
        $("#social_lightbox_likes").text('');
        $("#useravatar").attr('src', '');
        $("#avatarusername").text('');
        $("#photodate").text('');
        $(".img-modal").fadeOut('slow');
    });

    function custom_set_data(current) {
        $("#social_lightbox_title").text($(current).data('username'));
        $("#social_lightbox_image").attr('src', $(current).data('image'));
        $("#description").text($(current).data('description'));
        $("#social_lightbox_likes").text($(current).data('likes'));
        $("#useravatar").attr('src', $(current).data('userimage'));
        $("#avatarusername").text($(current).data('username'));
        $("#photodate").text($(current).data('insertdate'));
    }

    function set_data(current_element) {

        custom_set_data(current_element);


        $(current_element).addClass('lightbox-current');
    }

    function get_data(direction) {

        var lenght = $('#gallery-container a').length;
        var current = $('.lightbox-current').closest('.gallery-items').index('.gallery-items');

        $('.gallery-items').eq(current).removeClass('lightbox-current');

        var next_item = (direction == 'left') ? --current : ++current;

        if (next_item < 0) {
            next_item = lenght - 1;
        }

        if (next_item >= lenght) {
            next_item = 0;
        }

        set_data($('.gallery-items').eq(next_item));
    }

    var page_like_callback = function(url, html_element) {
        
        var ajaxURL = '/wp-admin/admin-ajax.php';
        // here is where the request will happen
        jQuery.ajax({
            type: 'GET',
            url: ajaxURL, //ajax controller request
            data: {
                'action': 'paranix_update_like', //action invoked
                'fn': 'paranix_update_like', //function required
                'photo_url': url,
                'event': 'like'
            },
            dataType: "json",
            success: function(response) {
                $('.modal').modal('hide');
                btn.button('reset');
                return false;
            }
        });
    };
    var page_unlike_callback = function(url, html_element) {
        jQuery.ajax({
            type: 'GET',
            url: ajaxURL, //ajax controller request
            data: {
                'action': 'paranix_update_like', //action invoked
                'fn': 'paranix_update_like', //function required
                'photo_url': url,
                'event': 'unlike'
            },
            dataType: "json",
            success: function(response) {
                $('.modal').modal('hide');
                btn.button('reset');
                return false;
            }
        });
    };
    
    $(document).bind('FBSDKLoaded', function() {
        FB.Event.subscribe('edge.create', page_like_callback);
        FB.Event.subscribe('edge.remove', page_unlike_callback);
    });

});