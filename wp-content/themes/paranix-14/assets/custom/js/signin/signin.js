/* 
 * @author Marco Barrella
 */

jQuery(function($) {

    $("#lost-password-button").click(function() {
        $("#main-checkpage-content").slideUp();
        $("#lost-password").slideUp(function() {
            $(this).removeClass('hide');
        });
        return false;
    });

    $("#lost-password-back").click(function() {
        $("#main-checkpage-content").slideDown();
        $("#lost-password").slideDown(function() {
            $(this).addClass('hide');
        });
        return false;
    });

    $("#choose-registration-type-button").click(function() {
        $("#main-checkpage-content").slideUp();
        $("#choose-registration-type").slideUp(function() {
            $(this).removeClass('hide');
        });
        return false;
    });

    $("#choose-registration-type-button-back").click(function() {
        $("#main-checkpage-content").slideDown();
        $("#choose-registration-type").slideDown(function() {
            $(this).addClass('hide');
        });
        return false;
    });

});


