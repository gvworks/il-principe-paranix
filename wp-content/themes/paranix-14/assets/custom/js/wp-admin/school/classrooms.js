/* 
 * @author Marco Barrella
 */

jQuery(function($) {

    $(".addclassrooms").click(function(){
        var value = $(".paranix_classrooms_add").val();
        var teacher = $("#teachers").val();
        var teacher_name = $('#teachers option:selected').html();
        var span = $('<span>').attr('class', 'classroom_item')
                .append($('<input>').prop('type', 'text').attr('name', 'paranix_classrooms[]').val(value))
                .append($('<span>').text(teacher_name))
                .append($('<input>').prop('type', 'hidden').attr('name', 'paranix_classrooms_teacher[]').val(teacher))
                .append($('<a>').attr({'href': '#', 'class': 'removethis'}).text('x'));
        $("#classrooms").append(span);
        return false;
    });
    
    $(".removethis").live('click', function(){
        $(this).parent('span').remove();
        return false;
    });
});