/* 
 * @author Marco Barrella
 */

jQuery(function($) {

    $("#paranix_school_comune").autocomplete({
        source: function(request, response) {
            var ajaxURL = '/wp-admin/admin-ajax.php';
            // here is where the request will happen
            jQuery.ajax({
                type: 'GET',
                url: ajaxURL, //ajax controller request
                data: {
                    'action': 'paranix_data_load_comuni', //action invoked
                    'fn': 'paranix_data_load_comuni', //function required
                    'term': request.term//if of post required
                },
                dataType: "json",
                success: function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.label,
                            id: item.id
                        };
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            $('#paranix_school_comune_id').val(ui.item.id);
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        return $("<li>")
                .append("<a>" + item.label + "</a>")
                .appendTo(ul);
    };
});