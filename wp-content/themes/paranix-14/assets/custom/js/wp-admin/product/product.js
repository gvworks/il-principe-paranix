jQuery(function($){
    $("#product_shortcode_generator").on('change', function(){
        var shortcode = '[paranix-product id="'+$(this).val()+'" color="'+$("#product_shortcode_generator_color").val()+'"]';
        $("#paranix_shortcode_generate").text(shortcode);
    });
    
    $("#product_shortcode_generator_color").on('change', function(){
        var shortcode = '[paranix-product id="'+$("#product_shortcode_generator").val()+'" color="'+$(this).val()+'"]';
        $("#paranix_shortcode_generate").text(shortcode);
    });
});

