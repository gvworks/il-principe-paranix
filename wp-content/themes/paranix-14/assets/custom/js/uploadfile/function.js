 jQuery(function($){
    $("#multipleupload").uploadFile({
        url: "/wp-admin/admin-ajax.php?action=paranix_upload_photocontest&fn=paranix_upload_photocontest",
        multiple:true,
        fileName:"paranix_userimage",
        afterUploadAll:function()
        {
            localStorage.setItem('uploaded_photo', 1);
        }
    });
    
    $('.nav-tabs > li > a').bind('click', function (e) {
        var href = $(this).attr('href');
        if(href == '#photo-contest' && localStorage.getItem('uploaded_photo') == 1){
            var params = [
                "tab_opened=photo-contest"
            ];

            window.location.href = "http://" + window.location.host + window.location.pathname + '?' + params.join('&');
            localStorage.removeItem('uploaded_photo');
        }
    });
    
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
    
    $(".update_data").submit(function(){
        
        var btn = $(this).find('.send_data');
        btn.button('loading');
        var values = {};
        $.each($(this).serializeArray(), function(i, field) {
            values[field.name] = field.value;
        });
        
        var ajaxURL = '/wp-admin/admin-ajax.php';
        // here is where the request will happen
        jQuery.ajax({
            type: 'POST',
            url: ajaxURL, //ajax controller request
            data: {
                'action': 'paranix_update_photo', //action invoked
                'fn': 'paranix_update_photo', //function required
                'photo_id': values.photo_id,
                'childname': values.paranix_childname,//if of post required
                'photo_description': values.paranix_photo_description
            },
            dataType: "json",
            success: function(response) {
                $('.modal').modal('hide');
                btn.button('reset');
                return false;
            }
        });
        return false;
        
    });
    
    $(".remove-photo").click(function(){
        var item = $(this).parent().parent().parent();
        var r = confirm("Sei sicuro di voler cancellare la foto?");
        if (r == true) {
            var photo_id = $(this).data('target');
            var ajaxURL = '/wp-admin/admin-ajax.php';
            // here is where the request will happen
            jQuery.ajax({
                type: 'POST',
                url: ajaxURL, //ajax controller request
                data: {
                    'action': 'paranix_delete_photo', //action invoked
                    'fn': 'paranix_delete_photo', //function required
                    'photo_id': photo_id
                },
                dataType: "json",
                success: function(response) {
                    $(item).fadeOut("normal", function() {
                        $(this).remove();
                    });
                    return false;
                }
            });
        }
        else{
            return false;
        }
    });
});