<?php
/*
  Template Name: Upload User Photo
 */

if (!is_user_logged_in()) {
    wp_redirect(PARANIX_LOGIN_PAGE);
}

$paranix_current_user = wp_get_current_user();

if (Paranix_Core_LoaderManager::is_post()) {

    if ($_FILES['paranix_userimage']['name']) {

        echo 'entro';

        $paranix_attachment_id = paranix_photo_contest_upload_image(
                $_FILES['paranix_userimage'], $_POST['paranix_childname'], $_POST['paranix_photo_description']
        );

        if (is_int($paranix_attachment_id)) {
            paranix_photo_contest_set_metadata($paranix_attachment_id, $paranix_current_user->ID);
        }
    }
}

require_once dirname(__FILE__) . '/layouts/partials/header.php';
?>

<body>
    <?php require dirname(__FILE__) . '/layouts/partials/nav.php'; ?>
    <?php require_once dirname(__FILE__) . '/layouts/partials/carousel.php'; ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12 boxshadow padding-top-bottom">
            <?php
            if (!paranix_photo_contest_check_user($paranix_current_user->ID)):
                ?>
                <form accept-charset="UTF-8" class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="well">
                        <h3 class="bigh3title text_purple">Carica la tua foto!</h3>
                        <hr/>
                        <div class="media">
                            <a class="pull-left" href="#">
                                <img id="paranix_user_preview_image" class="media-object" src="http://www.gotcode.org/wow_images/avatar.jpg" alt="..." />
                                <br/>
                                <input type="file" id="paranix_userimage" name="paranix_userimage"/>
                                <?php wp_nonce_field('my_image_upload', 'my_image_upload_nonce'); ?>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    Se non hai adesso la foto, puoi caricarla in un secondo momento. <br/>
                                    Puoi anche scaricare la App <strong>"Principe Paranix"</strong> e caricare la foto dal tuo cellulare.
                                </h4>
                                <!-- Text input-->
                                <div class="control-group">
                                    <label class="control-label" for="paranix_childname">Nome del bambino</label>
                                    <div class="controls">
                                        <input id="paranix_childname" name="paranix_childname" type="text" placeholder="Nome del bambino" class="form-control">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="paranix_photo_description">Descrizione della foto</label>
                                    <div class="controls">
                                        <textarea id="paranix_photo_description"  name="paranix_photo_description" class="form-control"></textarea><br/>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="controls">
                                    <button class="btn btn-success pull-right">Carica foto</button>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                        </div>
                    </div>
                </form>
                <?php
            else:
                ?>
                <div class="well">
                    Hai già caricato una foto. <a href="/lista/">Vai alla lista</a>
                </div>
            <?php
            endif;
            ?>

        </div>
    </div>
    <hr/>
    <script type="text/javascript">
        $(function() {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#paranix_user_preview_image').attr('src', e.target.result).attr('width', 200);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#paranix_userimage").change(function() {
                readURL(this);
            });

            $("#paranix_comune").on('change', function() {
                $('#paranix_school_hide').show('slow').removeClass('hide');
            });
        });
    </script>
    <?php require dirname(__FILE__) . '/layouts/partials/footer.php'; ?>
</body>
</html>
