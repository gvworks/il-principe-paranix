<?php

/*
  Template Name: Warning
 */

$post_meta = paranix_photo_contest_get();

foreach ($post_meta as $meta) {
    $attachment = get_post($meta->post_id);
    $attachment_meta = get_post_meta($meta->post_id);
    
    $contest_photo_url = paranix_photo_contest_generate_url_by_id($attachment->ID);
    
    $call = file_get_contents('https://api.facebook.com/method/fql.query?query=select%20total_count,like_count,comment_count,share_count,click_count%20from%20link_stat%20where%20url=%27' . $contest_photo_url . '%27&format=json');
    $json = json_decode($call);
    
    update_post_meta($attachment->ID, '_paranix_photo_counter', $json[0]->total_count);
    
}
