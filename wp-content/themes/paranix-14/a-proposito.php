<?php
/*
  Template Name: A proposito
 */

require_once dirname(__FILE__) . '/layouts/partials/header.php';
?>
<body>
    <?php require dirname(__FILE__) . '/layouts/partials/nav.php'; ?>
    <?php require_once dirname(__FILE__) . '/layouts/partials/carousel.php'; ?>
    <hr/>
    <div class="container-fluid">
        <div class="col-md-12 boxshadow">
            <?php Paranix_Core_LayoutManager::include_partials('breadcrumb.php', 'products'); ?>
            <div class="container">
                <div class="col-md-12">
                    <img src="/assets/custom/images/paranix.png" class="col-xs-12 col-sm-4 col-md-3"/>
                    <span class="col-xs-12 col-sm-8 col-md-9">
                        <h3 class="bigh3title">A proposito di Paranix</h3>
                        <span class="general-text">
                            Paranix sa bene che liberare i capelli dai pidocchi dei vostri figli può essere un lavoro a tempo pieno, anche molto stressante.
                            Per questo Paranix è continuamente alla ricerca di nuovi modi per supportarvi nella soluzione di questo problema – che fa sfortunatamente parte della vita – in maniera semplice ed opportuna.
                        </span>
                    </span>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-4 product-list-item">
                    <div class="col-md-12 boxshadow boxrounded ohidden">
                        
                        <div class="col-xs-12 col-sm-6 col-md-12 text-center">
                            <a href="/prevenzione/">
                                <img src="/assets/custom/images/paranix-prevent.png" height="250"/>
                            </a>
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-md-12 product-list-desc">
                            <span class="col-xs-12 text-center">
                                <a href="/prevenzione/" class="ctalink">PREVENZIONE</a>
                            </span>
                            <ul class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 product-list-chars">
                                <li><b>PARANIX PREVENT</b></li>
                                <li style="color:#FFF;">&nbsp;</li>
                                <li style="color:#FFF;">&nbsp;</li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-4 product-list-item">
                    <div class="col-md-12 boxshadow boxrounded ohidden">
                        
                        <div class="col-xs-12 col-sm-6 col-md-12 text-center">
                            <a href="/trattamento/">
                                <img src="/assets/custom/images/paranix-trattamento.png" height="250"/>
                            </a>
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-md-12 product-list-desc">
                            <span class="col-xs-12 text-center">
                                <a href="/trattamento/" class="ctalink">TRATTAMENTO</a>
                            </span>
                            <ul class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 product-list-chars">
                                <li><b>PARANIX SHAMPOO</b></li>
                                <li><b>PARANIX SPRAY</b></li>
                                <li><b>PARANIX SENSITIVE</b></li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-4 product-list-item">
                    <div class="col-md-12 boxshadow boxrounded ohidden">
                        
                        <div class="col-xs-12 col-sm-6 col-md-12 text-center">
                            <a href="/trattamento/">
                                <img src="/assets/custom/images/paranix-post-trattamento.png" height="250"/>
                            </a>
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-md-12 product-list-desc">
                            <span class="col-xs-12 text-center">
                                <a href="/trattamento/" class="ctalink">POST TRATTAMENTO</a>
                            </span>
                            <ul class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 product-list-chars">
                                <li><b>PARANIX SHAMPOO</b></li>
                                <li><b>PARANIX DEFISSANTE</b></li>
                                <li><b>PARANIX PETTINE 3 IN 1</b></li>
                            </ul>
                        </div>
                        
                    </div>
                </div>

                <div class="col-md-12">&nbsp;</div>
                <p class="col-md-12 general-text">
                    Paranix offre una <span class="text-colored">gamma completa</span> di prodotti dalla prevenzione al trattamento, fino al post-trattamento.<br/>
                    Grazie alla sua azione di tipo meccanico ed alla sua formula <span class="text-colored">senza agenti chimici</span>, i pidocchi non possono sviluppare resistenze ai componenti del prodotto.
                </p>
                <p>&nbsp;</p>
                <p class="col-md-12">
                    <strong>
                        Paranix Trattamento Spray, Paranix Trattamento Shampoo, Paranix Sensitive e Paranix Pettine sono dispositivi medici CE.<br/>
                        Leggere attentamente le avvertenze e le istruzioni per l'uso. Autorizzazione del 18.08.2014.
                    </strong>
                </p>
                <div class="col-md-12">&nbsp;</div>
            </div>
        </div>
    </div>
    <hr/>
    <?php require dirname(__FILE__) . '/layouts/partials/footer.php'; ?>
</body>
</html>
