<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
if(  file_exists( $wp_config_local = dirname( __FILE__).'/wp-config-local.php' ) ){
	require_once $wp_config_local;
}
else{
    define('DB_NAME', 'paranix');

    /** MySQL database username */
    define('DB_USER', 'paranix');

    /** MySQL database password */
    define('DB_PASSWORD', 'cristian121');

    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8');

    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');
    
    define('WP_ENV', 1);

}
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JAIF7Zk*WsF{lk|t Y;mvHGN6pfHUO7Dd5] x!!O-;MTU$;||zBWl(2KZ!,nVXa|');
define('SECURE_AUTH_KEY',  '[}eL-d|J|M4I)B~<rhh$[uGLON|F9(En&1W8$ TdOshM^T&NhZA5vBFMa%>|%hqf');
define('LOGGED_IN_KEY',    '}frpZMO9|o[hrx]__K?JE6]rCvh 1A~d.gT}  wbzHY<8BJiN!xnjw8g0pSP$yXu');
define('NONCE_KEY',        '06z,58NA]<+a+4d$kL*|qa4x4u]90u}_ N|#sOR=8+0@IpT:X?l|r+[=tN=bo>Vi');
define('AUTH_SALT',        '2)9`%ZO;+ G}|JPU8]y 4!eTcHJtp6eMTjp-;|P^&&sAypJh(d||]>6k,&P;8UD_');
define('SECURE_AUTH_SALT', 'T>* *LJG=@pSCkRSkHQo-|DJP^H&vlmB;)[|+vS[23R!_;c0Q8J</6-5+Z|S /bn');
define('LOGGED_IN_SALT',   'PvY}1SWl$J;S-v>EMi| kZSB#YA;NMH~4@!tR$23nP3;.@7xha1|-yoVxSos[d[H');
define('NONCE_SALT',       'tBLwl%:y%3b.ady?NEWXPO-9&6W_^DyVr`>q{`OYvC3iEo:,mXUx8)#d(`#++J|<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'prnx14_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'it_IT');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define( 'UPLOADS', 'uploads' );

define( 'WP_POST_REVISION',  3 );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
